import { Toast } from '../plugins/toast';

export class ToastHelper {
	static $inject = ['$translate', 'Toast'];
	constructor(
		private $translate: angular.translate.ITranslateService,
		private toast: Toast) {
	}

	notifyReportSaved(): void {
		let msg = this.$translate.instant('reportsaved');
		this.toast.showLongBottom(msg);
	}

	notifyReportDeleted(): void {
		let msg = this.$translate.instant('reportdeleted');
		this.toast.showShortTop(msg);
	}

	notifyReportPublished(): void {
		let msg = this.$translate.instant('reportpublished');
		this.toast.showLongBottom(msg);
	}

	notifyIsPublished(): void {
		let msg = this.$translate.instant('ispublished');
		this.toast.showLongBottom(msg);
	}

	notifyAccountSaved(): void {
		let msg = this.$translate.instant('account.created');
		this.toast.showLongBottom(msg);
	}

	notifyAccountDuplicate(): void {
		let msg = this.$translate.instant('username.exists');
		this.toast.showLongBottom(msg);
	}

	notifyFacebookProblem(): void {
		let msg = this.$translate.instant('facebook.problem');
		this.toast.showLongBottom(msg);
	}

	notifyCopyToClipboard(): void {
		let msg = this.$translate.instant('msgcopy');
		this.toast.showLongBottom(msg);
	}
}
