import { Toast } from '../plugins/toast';

export class Rest {
	private root = "http://smartpolice.azurewebsites.net";
	static $inject = ['$http', '$translate', 'Toast'];
	constructor(
		private $http: angular.IHttpService,
		private $translate: angular.translate.ITranslateService,
		private toast: Toast) {
	}

	getRoot(): string {
		return this.root;
	}

	get<T>(url: string): angular.IPromise<T> {
		return this.$http.get(this.root + url)
			.then(response => <T>response.data)
			.catch((err: any) => {
				if (err.status === -1) {
					let msg = this.$translate.instant('nointernet');
					this.toast.showShortTop(msg);
				}
				else if (err.status === 403) {
					let msg = this.$translate.instant('noserver');
					this.toast.showShortTop(msg);
				}

				throw err.status;
			});
	}

	post<T>(url: string, data: any): angular.IPromise<T> {
		return this.$http.post(this.root + url, data)
			.then(response => <T>response.data)
			.catch((err: any) => {
				if (err.status === -1) {
					let msg = this.$translate.instant('nointernet');
					this.toast.showShortTop(msg);
				}
				else if (err.status === 403) {
					let msg = this.$translate.instant('noserver');
					this.toast.showShortTop(msg);
				}

				throw err.status;
			});
	}
}
