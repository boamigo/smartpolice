import { Report } from "../dto/report";
import { Rest } from './rest';

export class Sitemap {
	report: Report;
	pageNum = 1;

	static $inject = ['$location', '$routeParams', '$translate', 'Rest'];
	constructor(
		private $location: angular.ILocationService,
		private $routeParams: angular.route.IRouteParamsService,
		private $translate: angular.translate.ITranslateService,
		private rest: Rest) {
	}

	getBackendRoot(): string {
		return this.rest.getRoot();
	}

	getLang(): string {
		return this.$translate.use();
	}

	getPath(): string {
		return this.$location.path();
	}

	queryString(): any {
		return this.$location.search();
	}

	getParams(): any {
		return this.$routeParams;
	}

	gotoHome(): void {
		this.$location.path('/home/index');
	}

	gotoSearch(): void {
		this.$location.path('/home/search');
	}

	gotoReportAdd(): void {
		this.report = null;
		this.$location.path('/report/add');
	}

	gotoReportEdit(item: Report): void {
		this.report = item;
		this.$location.path('/report/edit');
	}

	gotoReportView(item: Report): void {
		this.report = item;
		this.$location.path('/report/view');
	}

	gotoReportList(): void {
		this.$location.path('/report/list');
	}

	gotoReportServer(): void {
		this.$location.path('/report/server');
	}

	exit(): void {
		(<any>navigator).app.exitApp();
	}
}
