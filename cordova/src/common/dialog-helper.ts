import { Car } from "../dto/car";

export class DialogHelper {
	private currentDialog: any;

	static $inject = ['$uibModal'];
	constructor(
		private $uibModal: any) {
	}

	setDialog($uibModalInstance: any): void {
		this.currentDialog = $uibModalInstance;
		this.currentDialog.result.then(() => this.currentDialog = null, () => this.currentDialog = null);
	}

	closeDialog(): boolean {
		if (this.currentDialog) {
			this.currentDialog.close();
			this.currentDialog = null;
			return true;
		}

		return false;
	}

	openCarDialog(imageSrc: string): angular.IPromise<Car> {
		let dialog = this.$uibModal.open({
			animation: true,
			component: 'carDialog',
			resolve: { car: () => null, imageSrc: () => imageSrc }
		});
		this.setDialog(dialog);
		return dialog.result;
	}

	openEditCarDialog(car: Car): angular.IPromise<Car> {
		let dialog = this.$uibModal.open({
			animation: true,
			component: 'carDialog',
			resolve: { car: () => car, imageSrc: () => null }
		});
		this.setDialog(dialog);
		return dialog.result;
	}

	openZoomCarDialog(car: Car): void {
		let dialog = this.$uibModal.open({
			animation: true,
			component: 'zoomDialog',
			resolve: { imageSrc: () => car.imageSrc }
		});
		this.setDialog(dialog);
	}

	openLangDialog(): void {
		let dialog = this.$uibModal.open({
			animation: true,
			component: 'langDialog',
			resolve: { }
		});
		this.setDialog(dialog);
	}
}
