import { Account } from '../dto/account';
import { Street } from '../dto/street';
import { Rest } from './rest';
import * as moment from 'moment';

export interface IFadeListener {
	onFadeIn(): void;
	onFadeOut(): void;
}

export class AppHelper {
	busy: boolean;

	static $inject = ['$rootScope', '$q'];
	constructor(
		private $rootScope: angular.IRootScopeService,
		private $q: angular.IQService) {
	}

	subscribeFadeListener(listener: IFadeListener): void {
		this.$rootScope.$on('fade-in', () => listener.onFadeIn());
		this.$rootScope.$on('fade-out', () => listener.onFadeOut());
	}

	fadeIn(): void {
		this.busy = true;
		this.$rootScope.$emit('fade-in');
	}

	fadeOut(): void {
		this.busy = false;
		this.$rootScope.$emit('fade-out');
	}

	setUser(user: Account): void {
		localStorage.setItem('CUR_USER', JSON.stringify(user));
	}

	clearUser(): void {
		localStorage.removeItem('CUR_USER');
	}

	getUser(): Account {
		let text = localStorage.getItem('CUR_USER');
		if (!text) {
			return null;
		}

		return JSON.parse(text);
	}

	resolve<T>(): angular.IPromise<T> {
		return this.$q.resolve<T>(null);
	}

	formatDate(value: Date): string {
		return moment(value).format('DD/MM/YYYY');
	}

	parseDate(value: string): Date {
		return moment(value, 'DD/MM/YYYY').toDate();;
	}

	formatCurrentTime(): string {
		return moment().format('HH:mm');
	}

	getNewImageFileName(): string {
		return moment().format('YYYYMMDD_HHmmss.jpg');
	}

	filterStreets(q: string, streets: Street[]): Street[] {
		q = q.toLowerCase();
		let priority1: Street[] = [];
		let priority2: Street[] = [];
		for(let item of streets) {
			let text = item.name.toLowerCase();
			let added = false;
			let parts = text.split(' ');
			for(let part of parts) {
				if (part.indexOf(q) === 0) {
					priority1.push(item);
					added = true;
					break;
				}
			}

			if (added) {
				continue;
			}

			if (text.indexOf(q) !== -1) {
				priority2.push(item);
			}
		}

		priority1 = priority1.sort((a, b) => a.pop - b.pop);
		let filtered: Street[] = [];
		if (priority1.length > 0) {
			for(let item of priority1) {
				if (filtered.length >= 6) {
					break;
				}

				filtered.push(item);
			}
		}

		if (filtered.length < 6 && priority2.length > 0) {
			priority2 = priority2.sort((a, b) => a.pop - b.pop);
			for(let item of priority2) {
				if (filtered.length >= 6) {
					break;
				}

				filtered.push(item);
			}
		}

		return filtered;
	}
}
