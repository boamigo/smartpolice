/// <reference path="../../import/cordova-plugin-crop.d.ts" />
export class Camera {
	static $inject = ['$q'];
	constructor(
		private $q: angular.IQService) {
	}

	takePicture(): angular.IPromise<string> {
		let defered = this.$q.defer<string>();
		let camera = navigator['camera'];
		camera.getPicture(
			uri => defered.resolve(uri),
			err => defered.reject(err),
			{
				destinationType: camera.DestinationType.FILE_URI,
				mediaType: camera.MediaType.PICTURE,
				sourceType: camera.PictureSourceType.CAMERA
			});
		return defered.promise;
	}

	fromGallery(): angular.IPromise<string> {
		let defered = this.$q.defer<string>();
		let camera = navigator['camera'];
		camera.getPicture(
			uri => defered.resolve(uri),
			err => defered.reject(err),
			{
				destinationType: camera.DestinationType.FILE_URI,
				mediaType: camera.MediaType.PICTURE,
				sourceType: camera.PictureSourceType.PHOTOLIBRARY
			});
		return defered.promise;
	}

	cropImage(uri: string): angular.IPromise<string> {
		let defered = this.$q.defer<string>();
		window['plugins'].crop(
			newUri => defered.resolve(newUri),
			err => defered.reject('CROP'),
			uri,
			{ keepingAspectRatio: false });
		return defered.promise;
	}
}
