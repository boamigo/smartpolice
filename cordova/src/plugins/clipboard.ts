export class Clipboard {
	static $inject = ['$q'];
	constructor(
		private $q: angular.IQService) {
	}

	copy(text: string): angular.IPromise<void> {
		let defered = this.$q.defer<void>();
		cordova.plugins['clipboard'].copy(
			text,
			() => defered.resolve(),
			err => defered.reject(err));
		return  defered.promise;
	}
}
