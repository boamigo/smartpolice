/// <reference path="../../import/cordova-plugin-file-transfer.d.ts" />
export class Upload {
	private host = 'http://smartpolice.azurewebsites.net';

	static $inject = ['$q'];
	constructor(
		private $q: angular.IQService) {
	}

	uploadImage(path: string, reportId: string, carId: string): angular.IPromise<void> {
		let defered = this.$q.defer<void>();
		let options: FileUploadOptions = {
			fileKey: 'file',
			
			fileName: carId + '.jpg',
			mimeType: 'image/jpg',
			httpMethod: 'POST',
			params: { reportId: reportId, carId: carId }
		};
		let transfer = new FileTransfer();
		transfer.upload(
			path,
			encodeURI(this.host + '/api/report/upload'),
			response => defered.resolve(),
			err => defered.reject('File Upload Failed'),
			options);
		return defered.promise;
	}
}
