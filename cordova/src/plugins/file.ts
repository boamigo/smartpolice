/// <reference path="../../import/cordova-plugin-file.d.ts" />
export class File {

	static $inject = ['$q'];
	constructor(
		private $q: angular.IQService) {
	}

	getDirectory(path: string): angular.IPromise<DirectoryEntry> {
		let defered = this.$q.defer<DirectoryEntry>();
		window.resolveLocalFileSystemURL(
			path,
			entry => defered.resolve(<DirectoryEntry>entry),
			err => defered.reject('File Permission Denied'));
		return defered.promise;
	}

	getFile(path: string): angular.IPromise<FileEntry> {
		let defered = this.$q.defer<FileEntry>();
		window.resolveLocalFileSystemURL(
			path,
			entry => defered.resolve(<FileEntry>entry),
			err => defered.reject('File Permission Denied'));
		return defered.promise;
	}

	getFileInDir(folder: DirectoryEntry, name: string): angular.IPromise<FileEntry> {
		let defered = this.$q.defer<FileEntry>();
		folder.getFile(
			name,
			{ create: true, exclusive: false },
			entry => defered.resolve(<FileEntry>entry),
			err => defered.reject('File Permission Denied'));
		return defered.promise;
	}

	readJson(entry: FileEntry): angular.IPromise<any> {
		let defered = this.$q.defer<any>();
		entry.file(blob => {
			let reader = new FileReader();
			reader.onloadend = () => {
				let data = reader.result ? JSON.parse(<string>reader.result) : null;
				defered.resolve(data);
			};
			reader.readAsText(blob);
		},
		error => defered.reject('Failed reading from file: ' + error));
		return defered.promise;
	}

	writeJson(entry: FileEntry, data: any): angular.IPromise<void> {
		let defered = this.$q.defer<void>();
		entry.createWriter(writer => {
			writer.onwriteend = () => defered.resolve();
			writer.onerror = () => defered.reject('Failed writing to file');
			let blob = new Blob([JSON.stringify(data,)], {type : 'application/json'});
			writer.write(blob);
		});
		return defered.promise;
	}

	deleteFile(file: Entry): angular.IPromise<void> {
		let defered = this.$q.defer<void>();
		file.remove(() => defered.resolve(), err => defered.reject(err));
		return defered.promise;
	}

	getRootDirectory(): angular.IPromise<DirectoryEntry> {
		let defered = this.$q.defer<DirectoryEntry>();
		window.requestFileSystem(
			LocalFileSystem.PERSISTENT,
			0,
			fs => defered.resolve(fs.root),
			err =>defered.reject('Permission Denied'));
		return defered.promise;
	}

	getAppFolder(root: DirectoryEntry): angular.IPromise<DirectoryEntry> {
		let defered = this.$q.defer<DirectoryEntry>();
		root.getDirectory(
			'smartpolice',
			{ create: true, exclusive: false },
			entry => defered.resolve(entry),
			() => defered.reject('Create Directory Fail'));
		return defered.promise;
	}

	moveFile(file: Entry, newName, dest: DirectoryEntry): angular.IPromise<Entry> {
		let defered = this.$q.defer<Entry>();
		file.moveTo(
			dest,
			newName,
			entry => defered.resolve(entry),
			err => defered.reject(err));
		return defered.promise;
	}

	moveFileToAppFolder(file: Entry, newName: string): angular.IPromise<Entry> {
		return this.getRootDirectory()
			.then(root => this.getAppFolder(root))
			.then(appFolder => this.moveFile(file, newName, appFolder));
	}

	getExternalPath(entry: Entry): string {
		return cordova.file.externalRootDirectory + entry.fullPath.substr(1);
	}
}
