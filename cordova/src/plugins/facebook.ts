export class Facebook {
	static $inject = ['$q'];
	constructor(
		private $q: angular.IQService) {
	}

	login(): angular.IPromise<any> {
		let defered = this.$q.defer<any>();
		window['facebookConnectPlugin'].login(
			['public_profile'],
			response => defered.resolve(response),
			err => defered.reject(err));
		return defered.promise;
	}

	logout(): angular.IPromise<void> {
		let defered = this.$q.defer<void>();
		window['facebookConnectPlugin'].logout(
			response => defered.resolve(),
			err => defered.reject(err));
		return defered.promise;
	}

	getLoginStatus(): angular.IPromise<any> {
		let defered = this.$q.defer<any>();
		window['facebookConnectPlugin'].getLoginStatus(
			response => defered.resolve(response),
			err => defered.reject(err));
		return defered.promise;
	}

	getName(): angular.IPromise<any> {
		let defered = this.$q.defer<any>();
		window['facebookConnectPlugin'].api(
			'/me?fields=id,name',
			['public_profile'],
			response => defered.resolve(response),
			err => defered.reject(err));
			return defered.promise;
	}
}
