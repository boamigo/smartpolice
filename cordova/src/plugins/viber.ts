export class Viber {
	static $inject = ['$q'];
	constructor(
		private $q: angular.IQService) {
	}

	shareText(text: string): angular.IPromise<string> {
		let defered = this.$q.defer<string>();
		window['plugins'].viber.shareText(text,
			res => {
				defered.resolve(res)
			},
			err => {
				defered.reject(err);
			});
		return defered.promise;
	}

	shareImages(pathList: string[]): angular.IPromise<string> {
		let defered = this.$q.defer<string>();
		window['plugins'].viber.shareImages(pathList,
			res => {
				defered.resolve(res)
			},
			err => {
				defered.reject(err);
			});
		return defered.promise;
	}
}
