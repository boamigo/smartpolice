export class Toast {
	showShortTop(msg: string): void {
		window['plugins'].toast.showShortBottom(msg);
	}

	showLongBottom(msg: string): void {
		window['plugins'].toast.showLongBottom(msg);
	}
}
