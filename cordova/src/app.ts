import * as angular from 'angular';

import { AppHelper } from './common/app-helper';
import { DialogHelper } from './common/dialog-helper';
import { Rest } from './common/rest';
import { Sitemap } from './common/sitemap';
import { ToastHelper } from './common/toast-helper';

import { Camera } from './plugins/camera';
import { Facebook } from './plugins/facebook';
import { File } from './plugins/file';
import { Toast } from './plugins/toast';
import { Upload } from './plugins/upload';
import { Viber } from './plugins/viber';
import { Clipboard } from './plugins/clipboard';

import { BrandService } from './services/brand-service';
import { StreetService } from './services/street-service';
import { ReportService } from './services/report-service';
import { AccountService } from './services/account-service';
import { CommentService } from './services/comment-service';

import { BackdropComponent } from './components/backdrop/backdrop.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ReportAddComponent } from './components/report-add/report-add.component';
import { ReportServerComponent } from './components/report-server/report-server.component';
import { ReportListComponent } from './components/report-list/report-list.component';
import { ReportEditComponent } from './components/report-edit/report-edit.component';
import { ReportViewComponent } from './components/report-view/report-view.component';
import { CarDialogComponent } from './components/car-dialog/car-dialog.component';
import { ZoomDialogComponent } from './components/zoom-dialog/zoom-dialog.component';
import { LangDialogComponent } from './components/lang-dialog/lang-dialog.component';
import { SearchComponent } from './components/search/search.component';


export class Application {
	private static appModule : angular.IModule;
	public static create() {
		Application.appModule = angular.module('smartpolice', ['ngRoute', 'ui.bootstrap', 'ngAnimate', 'pascalprecht.translate']);
		Application.appModule.config(['$routeProvider', '$locationProvider', ($routeProvider: angular.route.IRouteProvider, $locationProvider: angular.ILocationProvider) => {
			$locationProvider.html5Mode({ enabled: true, requireBase: false });
			$routeProvider
			.when('/home/index', { template: '<home></home>' })
			.when('/home/about', { template: '<about></about>' })
			.when('/home/profile', { template: '<profile></profile>' })
			.when('/home/search', { template: '<search></search>' })
			.when('/report/add', { template: '<report-add></report-add>' })
			.when('/report/server', { template: '<report-server></report-server>' })
			.when('/report/view', { template: '<report-view></report-view>' })
			.when('/report/edit', { template: '<report-edit></report-edit>' })
			.when('/report/list', { template: '<report-list></report-list>' })
			.otherwise({ template: '<home></home>' });
		}]);
		Application.appModule.config(['$translateProvider', $translateProvider => {
			$translateProvider.useStaticFilesLoader({
				prefix: 'l10n/locale-',
				suffix: '.json'
			});
			let langKey = localStorage.getItem('NG_TRANSLATE_LANG_KEY');
			$translateProvider.preferredLanguage(langKey ? langKey : 'ro');
			$translateProvider.useSanitizeValueStrategy('escape');
		}]);
	}

	public static service(name: string, serviceConstructor: angular.Injectable<Function>) {
		Application.appModule.service(name, serviceConstructor);
	}

	public static controller(name: string, controllerConstructor: angular.Injectable<angular.IControllerConstructor>) {
		Application.appModule.controller(name, controllerConstructor);
	}

	public static filter(name: string, filterFactoryFunction: angular.Injectable<Function>) {
		Application.appModule.filter(name, filterFactoryFunction);
	}

	public static directive(name: string, directiveConstructor: angular.Injectable<any>) {
		Application.appModule.directive(name, directiveConstructor);
	}

	public static component(name: string, options: angular.IComponentOptions) {
		Application.appModule.component(name, options);
	}

	public static component2(componentClass: any): void {
		Application.appModule.component(componentClass.Name, {
			templateUrl: componentClass.Html,
			controller: componentClass
		});
	}
}

(():void => {
	Application.create();

	Application.service('AppHelper', AppHelper);
	Application.service('DialogHelper', DialogHelper);
	Application.service('Rest', Rest);
	Application.service('Sitemap', Sitemap);
	Application.service('ToastHelper', ToastHelper);

	Application.service('Camera', Camera);
	Application.service('Facebook', Facebook);
	Application.service('File', File);
	Application.service('Toast', Toast);
	Application.service('Upload', Upload);
	Application.service('Viber', Viber);
	Application.service('Clipboard', Clipboard);

	Application.service('BrandService', BrandService);
	Application.service('StreetService', StreetService);
	Application.service('ReportService', ReportService);
	Application.service('AccountService', AccountService);
	Application.service('CommentService', CommentService);

	Application.service('AppHelper', AppHelper);

	Application.component('backdrop', BackdropComponent.Meta);
	Application.component('navbar', NavbarComponent.Meta);
	Application.component('about', AboutComponent.Meta);
	Application.component('home', HomeComponent.Meta);
	Application.component('profile', ProfileComponent.Meta);
	Application.component('reportAdd', ReportAddComponent.Meta);
	Application.component('reportServer', ReportServerComponent.Meta);
	Application.component('reportList', ReportListComponent.Meta);
	Application.component('reportEdit', ReportEditComponent.Meta);
	Application.component('reportView', ReportViewComponent.Meta);
	Application.component2(SearchComponent);
	Application.component('carDialog', CarDialogComponent.Meta);
	Application.component('zoomDialog', ZoomDialogComponent.Meta);
	Application.component('langDialog', LangDialogComponent.Meta);
})();
