import { Account } from '../dto/account';
import { Rest } from '../common/rest';

export class AccountService {

	static $inject = ['Rest'];
	constructor(
		private rest: Rest) {
	}

	save(data: Account): angular.IPromise<Account> {
		return this.rest.post('/api/account/save', data);
	}

	getByFacebook(facebookId: string): angular.IPromise<Account> {
		return this.rest.get('/api/account/getbyfb/' + facebookId);
	}
}
