import { Comment } from '../dto/comment';
import { Rest } from '../common/rest';

export class CommentService {

	static $inject = ['Rest'];
	constructor(
		private rest: Rest) {
	}

	save(data: Comment): angular.IPromise<Comment> {
		return this.rest.post('/api/comment/save', data);
	}

	getByReport(reportId: string): angular.IPromise<Comment[]> {
		return this.rest.get('/api/comment/getbyreport/' + reportId);
	}
}
