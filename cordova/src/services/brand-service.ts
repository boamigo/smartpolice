export class BrandService {
	static $inject = ['$http'];
	constructor(private $http: angular.IHttpService) {
	}

	getBrands(): angular.IPromise<string[]> {
		return this.$http.get('brands.json')
			.then(response => <string[]>response.data);
	}
}
