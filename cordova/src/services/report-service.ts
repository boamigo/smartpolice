import { File } from '../plugins/file';
import { Rest } from '../common/rest';
import { Report } from '../dto/report';
import { Car } from '../dto/car';
import { Page } from '../dto/page';
import * as shortid from 'shortid';

export class ReportService {
	static $inject = ['File', 'Rest'];
	constructor(
		private file: File,
		private rest: Rest) {
	}

	getLocalReports(): angular.IPromise<Report[]> {
		return this.getReportsFile()
			.then(reportsFile => this.file.readJson(reportsFile))
			.then(json => <Report[]>json);
	}

	saveLocalReport(item: Report): angular.IPromise<string> {
		let newId = '';
		return this.getReportsFile()
		.then(reportsFile => this.file.readJson(reportsFile))
		.then(json => {
			let reports: Report[] = json;
			if (!reports) {
				reports = [];
			}

			newId = shortid.generate();
			item.id = newId;
			reports.push(item);
			return reports;
		})
		.then(reports => {
			return this.getReportsFile()
				.then(reportsFile => this.file.writeJson(reportsFile, reports));
		})
		.then(() => newId);
	}

	updateLocalReport(item: Report): angular.IPromise<string> {
		return this.getReportsFile()
		.then(reportsFile => this.file.readJson(reportsFile))
		.then(json => {
			let reports: Report[] = json;
			for(let rep of reports) {
				if (rep.id === item.id) {
					rep.address = item.address;
					rep.date = item.date;
					rep.time = item.time;
					rep.notes = item.notes;
					rep.cars = item.cars;
					rep.isPublished = item.isPublished;
					break;
				}
			}

			return reports;
		})
		.then(reports => {
			return this.getReportsFile()
				.then(reportsFile => this.file.writeJson(reportsFile, reports));
		})
		.then(() => item.id);
	}

	deleteLocalReport(item: Report): angular.IPromise<void> {
		return this.getReportsFile()
		.then(reportsFile => this.file.readJson(reportsFile))
		.then(json => {
			let reports: Report[] = json;
			if (!reports) {
				reports = [];
			}

			let index = 0;
			let found = false;
			for(let rep of reports) {
				if (rep.id === item.id) {
					found = true;
					break;
				}

				index++;
			}

			if (found) {
				reports.splice(index, 1);
			}

			return reports;
		})
		.then(reports => {
			return this.getReportsFile()
				.then(reportsFile => this.file.writeJson(reportsFile, reports));
		});
	}

	publishReport(data: Report): angular.IPromise<Report> {
		return this.rest.post('/api/report/publish', data);
	}

	getServerReports(pageIndex: number): angular.IPromise<Page<Report>> {
		return this.rest.get('/api/report/getpage/' + pageIndex);
	}

	searchCar(plate: string): angular.IPromise<Car[]> {
		return this.rest.get('/api/report/searchcar/' + plate);
	}

	private getReportsFile(): angular.IPromise<FileEntry> {
		return this.file.getRootDirectory()
			.then(root => this.file.getAppFolder(root))
			.then(appFolder => this.file.getFileInDir(appFolder, 'reports.json'));
	}
}
