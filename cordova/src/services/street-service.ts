import { Street } from '../dto/street';

export class StreetService {
	static $inject = ['$http'];
	constructor(private $http: angular.IHttpService) {
	}

	getStreets(): angular.IPromise<Street[]> {
		return this.$http.get('streets.json')
			.then(response => <Street[]>response.data);
	}
}
