export interface Car {
	id: string;
	smImageSrc: string;
	imageSrc: string;
	brand: string;
	plate: string;
	reportId?: string;
}
