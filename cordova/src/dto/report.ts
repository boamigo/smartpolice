import { Car } from './car';

export interface Report {
	id: string;
	address: string;
	date: string;
	time: string;
	notes: string;
	cars: Car[];
	isPublished: boolean;
}
