export class LangDialogComponent implements angular.IComponentController {
	static Meta: angular.IComponentOptions = {
		templateUrl: 'components/lang-dialog/lang-dialog.component.html',
		controller: LangDialogComponent,
		bindings: {
			resolve: '<',
			close: '&',
			dismiss: '&'
		}
	};

	close: () => void;
	dismiss: () => void;

	static $inject = ['$translate'];
	constructor (private $translate: angular.translate.ITranslateService) {
	}

	selectEn(): void {
		this.selectLang('en');
		this.close();
	}

	selectRo(): void {
		this.selectLang('ro');
		this.close();
	}

	selectRu(): void {
		this.selectLang('ru');
		this.close();
	}

	private selectLang(langKey: string): void {
		this.$translate.use(langKey);
		localStorage.setItem('NG_TRANSLATE_LANG_KEY', langKey);
	}
}
