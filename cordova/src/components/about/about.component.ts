export class AboutComponent implements angular.IComponentController {
	static Meta: angular.IComponentOptions = {
		templateUrl: 'components/about/about.component.html',
		controller: AboutComponent
	};

	static $inject = [];
	constructor () {
	}
}
