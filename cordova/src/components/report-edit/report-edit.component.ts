import { ReportAddComponent } from '../report-add/report-add.component';

export class ReportEditComponent implements angular.IComponentController {
	static Meta: angular.IComponentOptions = {
		templateUrl: 'components/report-add/report-add.component.html',
		controller: ReportAddComponent
	};
}
