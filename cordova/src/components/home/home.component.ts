import { Sitemap } from '../../common/sitemap';

export class HomeComponent implements angular.IComponentController {
	static Meta: angular.IComponentOptions = {
		templateUrl: 'components/home/home.component.html',
		controller: HomeComponent
	};

	static $inject = ['Sitemap'];
	constructor (private sitemap: Sitemap) {
	}

	report(): void {
		this.sitemap.gotoReportAdd();
	}

	savedReports(): void {
		this.sitemap.gotoReportList();
	}

	publishedReports(): void {
		this.sitemap.gotoReportServer();
	}
}
