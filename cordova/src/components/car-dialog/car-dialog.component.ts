import { Car } from '../../dto/car';
import { BrandService } from '../../services/brand-service';

export class CarDialogComponent implements angular.IComponentController {
	static Meta: angular.IComponentOptions = {
		templateUrl: 'components/car-dialog/car-dialog.component.html',
		controller: CarDialogComponent,
		bindings: {
			resolve: '<',
			close: '&',
			dismiss: '&'
		}
	};

	close: (any) => void;
	dismiss: () => void;
	brands: string[];
	brand: string;
	plate: string;
	imageSrc: string;
	resolve: any;

	static $inject = ['BrandService'];
	constructor (private brandService: BrandService) {
	}

	$onInit(): void {
		let car: Car = this.resolve.car;
		let imageSrc: string = this.resolve.imageSrc;
		if (car) {
			this.brand = car.brand;
			this.plate = car.plate;
			this.imageSrc = car.imageSrc;
		}
		else {
			this.brand = null;
			this.plate = null;
			this.imageSrc = imageSrc;
		}

		this.brandService.getBrands()
			.then(data => this.brands = data);
	}

	onOk($form: angular.IFormController): void {
		if (!$form.$valid) {
			$form.$submitted = true;
			return;
		}

		let car: Car = {
			id: undefined,
			smImageSrc: undefined,
			imageSrc: this.imageSrc,
			brand: this.brand,
			plate: this.plate
		};

		this.close({$value: car});
	}
}
