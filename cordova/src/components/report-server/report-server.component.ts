import { Report } from '../../dto/report';
import { AppHelper } from '../../common/app-helper';
import { Sitemap } from '../../common/sitemap';
import { ReportService } from '../../services/report-service';

export class ReportServerComponent implements angular.IComponentController {
	static Meta: angular.IComponentOptions = {
		templateUrl: 'components/report-server/report-server.component.html',
		controller: ReportServerComponent
	};

	pageNum: number;
	total: number;
	reports: Report[];

	static $inject = ['AppHelper', 'Sitemap', 'ReportService'];
	constructor (
		private appHelper: AppHelper,
		private sitemap: Sitemap,
		private reportService: ReportService) {
	}

	$onInit(): void {
		this.pageNum = this.sitemap.pageNum;
		this.loadPage();
	}

	loadPage(): angular.IPromise<void> {
		this.appHelper.fadeIn();
		this.sitemap.pageNum = this.pageNum;
		return this.reportService.getServerReports(this.pageNum - 1)
		.then(page => {
			this.reports = [];
			for(let report of page.items) {
				for(let car of report.cars) {
					car.smImageSrc = this.sitemap.getBackendRoot() + '/' + report.id + '/' + car.id + '_sm.jpg';
					car.imageSrc = this.sitemap.getBackendRoot() + '/' + report.id + '/' + car.id + '.jpg';
				}

				if (report.cars && report.cars.length > 0 && report.cars[0]) {
					report['defaultImageSrc'] = report.cars[0].smImageSrc;
				}

				this.reports.push(report);
			}

			this.total = page.total;
		})
		.finally(() => this.appHelper.fadeOut());
	}

	onReportClick(item: Report): void {
		this.sitemap.gotoReportView(item);
	}
}
