import { AppHelper } from '../../common/app-helper';
import { ToastHelper } from '../../common/toast-helper';
import { Facebook } from '../../plugins/facebook';
import { Account } from '../../dto/account';
import { AccountService } from '../../services/account-service';

export class ProfileComponent implements angular.IComponentController {
	static Meta: angular.IComponentOptions = {
		templateUrl: 'components/profile/profile.component.html',
		controller: ProfileComponent
	};

	facebookAttached: boolean;
	facebookId: string;
	facebookName: string;
	originalUserName: string;
	userName: string;
	readMode: boolean;

	static $inject = ['Facebook', 'AppHelper', 'ToastHelper', 'AccountService'];
	constructor (
		private facebook: Facebook,
		private appHelper: AppHelper,
		private toastHelper: ToastHelper,
		private accountService: AccountService) {
	}

	$onInit(): void {
		this.appHelper.fadeIn();
		this.facebook.getLoginStatus()
		.then(response => {
			this.facebookAttached = (response.status === 'connected');
		})
		.then(() => {
			if (this.facebookAttached) {
				return this.facebook.getName()
				.then(data => {
					this.facebookId = data.id;
					this.facebookName = data.name;
				});
			}
		})
		.then(() => {
			if (this.facebookAttached) {
				return this.accountService.getByFacebook(this.facebookId);
			}
		})
		.then(data => {
			if (data) {
				this.originalUserName = data.name;
				this.userName = data.name;
				this.appHelper.setUser(data);
				this.readMode = true;
			}
			else {
				this.readMode = false;
			}
		})
		.catch(err => this.toastHelper.notifyFacebookProblem())
		.finally(() => this.appHelper.fadeOut());
	}

	onAttachClick(): void {
		this.appHelper.fadeIn();
		this.facebook.login()
		.then(response => {
			this.facebookAttached = true;
			this.appHelper.clearUser();
		})
		.then(() => {
			if (this.facebookAttached) {
				return this.facebook.getName()
				.then(data => {
					this.facebookId = data.id;
					this.facebookName = data.name;
					this.userName = data.name;
				});
			}
		})
		.then(() => {
			if (this.facebookAttached) {
				return this.accountService.getByFacebook(this.facebookId);
			}
		})
		.then(data => {
			if (data) {
				this.originalUserName = data.name;
				this.userName = data.name;
				this.appHelper.setUser(data);
				this.readMode = true;
			}
		})
		.catch(err => this.toastHelper.notifyFacebookProblem())
		.finally(() => this.appHelper.fadeOut());
	}

	onDetachClick(): void {
		this.appHelper.fadeIn();
		this.facebook.logout()
		.then(response => {
			this.facebookAttached = false;
			this.facebookId = null;
			this.facebookName = null;
			this.userName = null;
			this.originalUserName = null;
			this.appHelper.clearUser();
			this.readMode = true;
		})
		.catch(err => this.toastHelper.notifyFacebookProblem())
		.finally(() => this.appHelper.fadeOut());
	}

	onSave($form: angular.IFormController): void {
		if (!this.facebookAttached || !$form.$valid) {
			$form.$submitted = true;
			return;
		}

		this.appHelper.fadeIn();
		this.appHelper.clearUser();
		let data: Account = {
			id: undefined,
			fbId: this.facebookId,
			name: this.userName
		};
		this.accountService.save(data)
		.then(dto => this.appHelper.setUser(dto))
		.then(() => {
			this.originalUserName = this.userName;
			this.readMode = true;
		})
		.then(() => this.toastHelper.notifyAccountSaved())
		.catch(errCode => {
			if (errCode === 520) {
				this.toastHelper.notifyAccountDuplicate();
			}
		})
		.finally(() => this.appHelper.fadeOut());
	}

	onEdit(): void {
		this.readMode = false;
	}

	onCancel($form: angular.IFormController): void {
		$form.$setPristine();
		$form.$setUntouched();
		this.readMode = true;
		this.userName = this.originalUserName;
	}
}
