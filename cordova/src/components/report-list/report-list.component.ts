import { Report } from '../../dto/report';
import { Sitemap } from '../../common/sitemap';
import { ToastHelper } from '../../common/toast-helper';
import { ReportService } from '../../services/report-service';

export class ReportListComponent implements angular.IComponentController {
	static Meta: angular.IComponentOptions = {
		templateUrl: 'components/report-list/report-list.component.html',
		controller: ReportListComponent
	};

	reports: Report[];

	static $inject = ['Sitemap', 'ToastHelper', 'ReportService'];
	constructor (
		private sitemap: Sitemap,
		private toastHelper: ToastHelper,
		private reportService: ReportService) {
	}

	$onInit(): void {
		this.reportService.getLocalReports()
		.then(data => {
			this.reports = [];
			for(let report of data) {
				if (report.cars && report.cars.length > 0 && report.cars[0]) {
					report['defaultImageSrc'] = report.cars[0].imageSrc;
				}

				this.reports.push(report);
			}

			this.reports.reverse();
		});
	}

	onReportClick(item: Report): void {
		this.sitemap.gotoReportEdit(item);
	}

	onDelete(item: Report): void {
		this.reportService.deleteLocalReport(item)
			.then(() => this.$onInit())
			.then(() => this.toastHelper.notifyReportDeleted());
	}
}
