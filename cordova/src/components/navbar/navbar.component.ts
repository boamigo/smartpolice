import { Sitemap } from '../../common/sitemap';
import { AppHelper } from '../../common/app-helper';
import { DialogHelper } from '../../common/dialog-helper';

export class NavbarComponent implements angular.IComponentController {
	static Meta: angular.IComponentOptions = {
		templateUrl: 'components/navbar/navbar.component.html',
		controller: NavbarComponent
	};

	isNavCollapsed = true;

	static $inject = ['$scope', 'Sitemap', 'AppHelper', 'DialogHelper'];
	constructor (
		private $scope: angular.IScope,
		private sitemap: Sitemap,
		private appHelper: AppHelper,
		private dialogHelper: DialogHelper) {
	}

	$onInit(): void {
		document.addEventListener('backbutton', () => this.backButtonClick(), false);
	}

	backButtonClick(): void {
		if (this.appHelper.busy) {
			return;
		}

		if(this.dialogHelper.closeDialog()) {
			return;
		}

		let currentPath = this.sitemap.getPath();
		if (currentPath === '/home/index' || currentPath === '/') {
			this.sitemap.exit();
		}
		else if (currentPath === '/report/edit') {
			this.sitemap.gotoReportList();
		}
		else if (currentPath === '/report/view') {
			this.sitemap.gotoReportServer();
		}
		else {
			this.sitemap.gotoHome();
		}

		this.$scope.$apply();
	}

	gotoHome(): void {
		if (this.appHelper.busy) {
			return;
		}

		this.hide();
		this.sitemap.gotoHome();
	}

	gotoSearch(): void {
		if (this.appHelper.busy) {
			return;
		}

		this.hide();
		this.sitemap.gotoSearch();
	}

	reportNow(): void {
		this.hide();
		this.sitemap.gotoReportAdd();
	}

	toggle(): void {
		if (this.appHelper.busy) {
			return;
		}

		this.dialogHelper.closeDialog();
		this.isNavCollapsed = !this.isNavCollapsed;
	}

	hide(): void {
		this.isNavCollapsed = true;
		this.dialogHelper.closeDialog();
	}

	exitApp(): void {
		this.sitemap.exit();
	}

	showLang(): void {
		this.hide();
		this.dialogHelper.openLangDialog();
	}

	langIcon(): string {
		return 'img/' + this.sitemap.getLang() + '.png';
	}
}
