import { Car } from '../../dto/car';
import { Street } from '../../dto/street';
import { Report } from '../../dto/report';
import { AppHelper } from '../../common/app-helper';
import { Sitemap } from '../../common/sitemap';
import { DialogHelper } from '../../common/dialog-helper';
import { ToastHelper } from '../../common/toast-helper';
import { Camera } from '../../plugins/camera';
import { Clipboard } from '../../plugins/clipboard';
import { File } from '../../plugins/file';
import { Upload } from '../../plugins/upload';
import { Viber } from '../../plugins/viber';
import { BrandService } from '../../services/brand-service';
import { StreetService } from '../../services/street-service';
import { ReportService } from '../../services/report-service';


const PAGE_SIZE = 10;
export class ReportAddComponent implements angular.IComponentController {
	static Meta: angular.IComponentOptions = {
		templateUrl: 'components/report-add/report-add.component.html',
		controller: ReportAddComponent
	};

	id: string;
	date = new Date();
	dateShow = false;
	time: string;
	address: string;
	notes: string;
	streets: Street[];
	cars: Car[] = [];
	cameraOpened = false;
	isPublished = false;
	pages = [];

	static $inject = ['AppHelper', 'Sitemap', 'DialogHelper', 'ToastHelper', 'Camera', 'Clipboard', 'File', 'Upload', 'Viber', 'BrandService', 'StreetService', 'ReportService'];
	constructor (
		private appHelper: AppHelper,
		private sitemap: Sitemap,
		private dialogHelper: DialogHelper,
		private toastHelper: ToastHelper,
		private camera: Camera,
		private clipboard: Clipboard,
		private file: File,
		private upload: Upload,
		private viber: Viber,
		private brandService: BrandService,
		private streetService: StreetService,
		private reportService: ReportService) {
	}

	$onInit(): void {
		let data = this.sitemap.report;
		if (data)
		{
			this.id = data.id;
			this.date = this.appHelper.parseDate(data.date);
			this.time = data.time;
			this.address = data.address;
			this.cars = data.cars;
			this.isPublished = data.isPublished;
			this.notes = data.notes;
			this.pages = new Array(Math.floor(this.cars.length / 10) + 1);
		}
		else {
			this.time = this.appHelper.formatCurrentTime();
		}

		this.streetService.getStreets()
			.then(data => this.streets = data);
	}

	takePicture(): void {
		this.cameraOpened = true;
		let fileName = this.appHelper.getNewImageFileName();
		let photoUri: string = null;
		this.file.getRootDirectory()
			.then(root => this.file.getAppFolder(root))
			.then(() => this.camera.takePicture())
			.then(uri => photoUri = uri)
			.then(() => this.camera.cropImage(photoUri))
			.then(cropUri => this.file.getFile(cropUri))
			.then(entry => this.file.moveFileToAppFolder(entry, fileName))
			.then(entry => this.file.getExternalPath(entry))
			.then(path => this.dialogHelper.openCarDialog(path))
			.then(car => {
				car.plate = car.plate.toUpperCase();
				this.cars.push(car);
				this.pages = new Array(Math.floor(this.cars.length / 10) + 1);
			});
	}

	fromGallery(): void {
		this.cameraOpened = true;
		let fileName = this.appHelper.getNewImageFileName();
		this.file.getRootDirectory()
			.then(root => this.file.getAppFolder(root))
			.then(() => this.camera.fromGallery())
			.then(uri => this.camera.cropImage(uri))
			.then(uri => this.file.getFile(uri))
			.then(entry => this.file.moveFileToAppFolder(entry, fileName))
			.then(entry => this.file.getExternalPath(entry))
			.then(path => this.dialogHelper.openCarDialog(path))
			.then(car => {
				car.plate = car.plate.toUpperCase();
				this.cars.push(car);
				this.pages = new Array(Math.floor(this.cars.length / 10) + 1);
			});
	}

	onShareText($form: angular.IFormController): void {
		if (!$form.$valid || this.cars.length === 0) {
			$form.$submitted = true;
			return;
		}

		let text = this.getText();
		this.viber.shareText(text);
	}

	onShareImages($form: angular.IFormController, pageIndex: number): void {
		if (!$form.$valid || this.cars.length === 0) {
			$form.$submitted = true;
			return;
		}

		let pathList: string[] = [];
		let startIndex = pageIndex * PAGE_SIZE;
		let nextPageStartIndex = startIndex + PAGE_SIZE;
		if (nextPageStartIndex > this.cars.length) {
			nextPageStartIndex = this.cars.length;
		}

		for (let i = startIndex; i < nextPageStartIndex; i++) {
			pathList.push(this.cars[i].imageSrc);
		}

		this.viber.shareImages(pathList);
	}

	onDeleteCar(car: Car): void {
		let index = this.cars.indexOf(car);
		this.cars.splice(index, 1);
		this.pages = new Array(Math.floor(this.cars.length / 10) + 1);
	}

	onZoomCar(car: Car): void {
		this.dialogHelper.openZoomCarDialog(car);
	}

	onEditCar(car: Car): void {
		this.dialogHelper.openEditCarDialog(car)
		.then(modified => {
			car.brand = modified.brand;
			car.plate = modified.plate.toUpperCase();
		});
	}

	onSave($form: angular.IFormController): void {
		if (!$form.$valid || this.cars.length === 0) {
			$form.$submitted = true;
			return;
		}

		this.saveOrUpdateLocalReport()
			.then(newId => this.id = newId)
			.then(() => this.toastHelper.notifyReportSaved());
	}

	onPublish($form: angular.IFormController): void {
		if (!$form.$valid || this.cars.length === 0) {
			$form.$submitted = true;
			return;
		}

		if (this.isPublished) {
			this.toastHelper.notifyIsPublished();
			return;
		}

		this.appHelper.fadeIn();
		let data: Report = {
			id: undefined,
			address: this.address,
			date: this.appHelper.formatDate(this.date),
			time: this.time,
			notes: this.notes,
			cars: [],
			isPublished: undefined
		};
		for(let car of this.cars) {
			data.cars.push({
				id: undefined,
				smImageSrc: undefined,
				imageSrc: undefined,
				brand: car.brand,
				plate: car.plate
			});
		}

		this.reportService.publishReport(data)
		.then(report => {
			let chain = this.appHelper.resolve<void>();
			let index = 0;
			for(let car of report.cars) {
				car.imageSrc = this.cars[index].imageSrc;
				chain = chain.then(() => this.upload.uploadImage(car.imageSrc, report.id, car.id));
				index++;
			}

			return chain;
		})
		.then(() => this.isPublished = true)
		.then(() => this.toastHelper.notifyReportPublished())
		.then(() => this.saveOrUpdateLocalReport())
		.then(id => this.id = id)
		.catch(err => alert('ERROR: ' + err))
		.finally(() => this.appHelper.fadeOut());
	}

	filterStreets(q: string): Street[] {
		return this.appHelper.filterStreets(q, this.streets);
	}

	copyText($form: angular.IFormController): void {
		if (!$form.$valid || this.cars.length === 0) {
			$form.$submitted = true;
			return;
		}

		this.clipboard.copy(this.getText())
			.then(() => this.toastHelper.notifyCopyToClipboard())
			.catch(err => alert(err));
	}

	private getText(): string {
		let text =
			this.appHelper.formatDate(this.date) + '   ' +
			this.time + '\n' +
			this.address + '\n' +
			'-------------------------\n';
		for(let car of this.cars) {
			let carText = car.brand + '   ' + car.plate + '\n';
			text += carText;
		}

		if (this.notes) {
			text += '-------------------------\n' + this.notes;
		}

		return text;
	}

	private saveOrUpdateLocalReport(): angular.IPromise<string> {
		let item: Report = {
			id: this.id,
			address: this.address,
			date: this.appHelper.formatDate(this.date),
			time: this.time,
			notes: this.notes,
			cars: [],
			isPublished: this.isPublished
		};
		for(let car of this.cars) {
			item.cars.push(car);
		}

		if (item.id) {
			return this.reportService.updateLocalReport(item);
		}
		else {
			return this.reportService.saveLocalReport(item);
		}
	}
}
