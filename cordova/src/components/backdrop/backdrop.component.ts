import { AppHelper, IFadeListener } from '../../common/app-helper';

export class BackdropComponent implements angular.IComponentController, IFadeListener {
	static Meta: angular.IComponentOptions = {
		templateUrl: 'components/backdrop/backdrop.component.html',
		controller: BackdropComponent
	};

	faded: boolean;

	static $inject = ['AppHelper'];
	constructor (private appHelper: AppHelper) {
	}

	$onInit(): void {
		this.faded = false;
		this.appHelper.subscribeFadeListener(this);
	}

	onFadeIn(): void {
		this.faded = true;
	}

	onFadeOut(): void {
		this.faded = false;
	}
}
