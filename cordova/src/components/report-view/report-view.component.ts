import { Car } from '../../dto/car';
import { Street } from '../../dto/street';
import { Report } from '../../dto/report';
import { Comment } from '../../dto/comment';
import { Account } from '../../dto/account';
import { AppHelper } from '../../common/app-helper';
import { DialogHelper } from '../../common/dialog-helper';
import { Sitemap } from '../../common/sitemap';
import { CommentService } from '../../services/comment-service';

export class ReportViewComponent implements angular.IComponentController {
	static Meta: angular.IComponentOptions = {
		templateUrl: 'components/report-view/report-view.component.html',
		controller: ReportViewComponent
	};

	id: string;
	date: string;
	time: string;
	address: string;
	notes: string;
	cars: Car[];
	comments: Comment[];
	commentText: string;
	user: Account;
	commentPosted = false;

	static $inject = ['AppHelper', 'DialogHelper', 'Sitemap', 'CommentService'];
	constructor (
		private appHelper: AppHelper,
		private dialogHelper: DialogHelper,
		private sitemap: Sitemap,
		private commentService: CommentService) {
	}

	$onInit(): void {
		this.appHelper.fadeIn();
		let data = this.sitemap.report;
		this.id = data.id;
		this.date = data.date;
		this.time = data.time;
		this.address = data.address;
		this.notes = data.notes;
		this.cars = data.cars;
		this.user = this.appHelper.getUser();
		this.commentService.getByReport(this.id)
			.then(data => this.comments = data)
			.finally(() => this.appHelper.fadeOut());
	}

	onZoom(car: Car): void {
		this.dialogHelper.openZoomCarDialog(car);
	}

	postComment($form: angular.IFormController): void {
		if (!$form.$valid || !this.user) {
			$form.$submitted = true;
			return;
		}

		this.appHelper.fadeIn();
		let data: Comment = {
			id: undefined,
			text: this.commentText,
			accountId: this.user.id,
			account: undefined,
			reportId: this.id
		};
		this.commentService.save(data)
			.then(() => this.commentPosted = true)
			.then(() => this.commentService.getByReport(this.id))
			.then(data => this.comments = data)
			.finally(() => this.appHelper.fadeOut());
	}
}
