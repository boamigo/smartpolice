import { Car } from '../../dto/car';
import { AppHelper } from '../../common/app-helper';
import { Sitemap } from '../../common/sitemap';
import { ReportService } from '../../services/report-service';
import { DialogHelper } from '../../common/dialog-helper';

export class SearchComponent implements angular.IComponentController {
	static Name = 'search';
	static Html = 'components/search/search.component.html';

	lastPlateTry: string;
	plate: string;
	cars: Car[];

	static $inject = ['AppHelper', 'Sitemap', 'ReportService', 'DialogHelper'];
	constructor (
		private appHelper: AppHelper,
		private sitemap: Sitemap,
		private reportService: ReportService,
		private dialogHelper: DialogHelper) {
	}

	$onInit(): void {
	}

	search($form: angular.IFormController): void {
		if (!$form.$valid) {
			$form.$submitted = true;
			return;
		}

		let text = this.plate.replace(/ /g,'');
		text = text.toUpperCase();
		if (text.length < 4) {
			$form.$submitted = true;
			return;
		}

		if (this.lastPlateTry === this.plate) {
			$form.$submitted = true;
			return;
		}

		this.lastPlateTry = this.plate;
		this.appHelper.fadeIn();
		this.reportService.searchCar(text)
		.then(data => {
			for(let car of data) {
				car.smImageSrc = this.sitemap.getBackendRoot() + '/' + car.reportId + '/' + car.id + '_sm.jpg';
				car.imageSrc = this.sitemap.getBackendRoot() + '/' + car.reportId + '/' + car.id + '.jpg';
			}

			this.cars = data;
		})
		.finally(() => this.appHelper.fadeOut());
	}

	onZoom(car: Car): void {
		this.dialogHelper.openZoomCarDialog(car);
	}
}
