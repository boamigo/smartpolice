export class ZoomDialogComponent implements angular.IComponentController {
	static Meta: angular.IComponentOptions = {
		templateUrl: 'components/zoom-dialog/zoom-dialog.component.html',
		controller: ZoomDialogComponent,
		bindings: {
		  resolve: '<',
		  close: '&',
		  dismiss: '&'
		}
	};

	resolve: any;
	imageSrc: string;

	static $inject = [];
	constructor () {
	}

	$onInit(): void {
		this.imageSrc = this.resolve.imageSrc;
	}
}
