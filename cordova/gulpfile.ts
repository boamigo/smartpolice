const fs = require('fs')
const gulp = require('gulp');
const del = require('del');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const tsProject = ts.createProject('tsconfig.json', {
	typescript: require('typescript') 
});
const browserify = require('browserify');
const source = require('vinyl-source-stream');

gulp.task('clean', (cb) => {
	return del(['src/**/*.js', '!src/libs/**/*.js'], cb);
});

gulp.task('compile', ['clean'], () => {
	var tsResult = gulp.src('src/**/*.ts')
		.pipe(tsProject());
	return tsResult.js
		.pipe(gulp.dest('src'));
});

gulp.task('browserify', ['compile'], () => {
	return browserify()
		.add('src/app.js')
		.exclude('angular')
		.ignore('angular')
		.bundle()
		.pipe(source('bundle.js'))
		.pipe(gulp.dest('src'));
});

gulp.task('libs', ['browserify'], () => {
	return gulp.src([
		'moment/min/moment.min.js',
		'angular/angular.min.js',
		'angular-route/angular-route.min.js',
		'angular-animate/angular-animate.min.js',
		'angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
		'angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
		'bootstrap/dist/css/bootstrap.css',
		'bootstrap/dist/fonts/**',
		'font-awesome/css/font-awesome.css',
		'font-awesome/fonts/**',
		'angular-translate/dist/angular-translate.min.js',
		'angular-translate/dist/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js'
	],
	{ cwd: 'node_modules/**' })
	.pipe(gulp.dest('src/libs'));
});

gulp.task('deploy_browserify', ['compile'], () => {
	return browserify()
		.add('src/app.js')
		.exclude('angular')
		.ignore('angular')
		.bundle()
		.pipe(source('bundle.js'))
		.pipe(gulp.dest('../smartpolice/www'));
});

gulp.task('deploy_content', ['deploy_browserify'], () => {
	return gulp.src(['src/**/*.*', '!src/**/*.ts', '!src/**/*.js'])
		.pipe(gulp.dest('../smartpolice/www'));
});

gulp.task('deploy_basehref', ['deploy_content'], () => {
	return new Promise((resolve, reject) => {
		let filePath = '../smartpolice/www/index.html';
		fs.readFile(filePath, 'utf8', (err, data) => {
			if (err) {
				console.log(err);
				reject(err);
			}

			let result = data.replace('<base href="/" />', '<base href="www" />');
			fs.writeFile(filePath, result, 'utf8', function (err) {
				if (err) {
					console.log(err);
					reject(err);
				}

				resolve();
			});
		});
	});
});

gulp.task('deploy_libs', ['deploy_basehref'], () => {
	return gulp.src([
		'moment/min/moment.min.js',
		'angular/angular.min.js',
		'angular-route/angular-route.min.js',
		'angular-animate/angular-animate.min.js',
		'angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
		'angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
		'bootstrap/dist/css/bootstrap.css',
		'bootstrap/dist/fonts/**',
		'font-awesome/css/font-awesome.css',
		'font-awesome/fonts/**',
		'angular-translate/dist/angular-translate.min.js',
		'angular-translate/dist/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js'
	],
	{ cwd: 'node_modules/**' })
	.pipe(gulp.dest('../smartpolice/www/libs'));
});


gulp.task('streets', (cb) => {
	let all = JSON.parse(fs.readFileSync('point.json', 'utf8'));
	let streets = [];
	let count = 0;
	for(let item of all) {
		if (item.type === 'street' && item.parent_name && item.parent_name.startsWith('Chi')) {
			streets.push(item.name);
			console.log(item.name);
			count ++;
		}
	}

	console.log('Total: ' + all.length);
	console.log('Streets: ' + count);
});