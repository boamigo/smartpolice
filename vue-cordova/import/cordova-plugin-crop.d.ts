declare namespace plugins {
	function crop(
		success: (uri: string) => void,
		failure: (err: any) => void,
		uri: string,
		options: any): void;
}

export as namespace plugins;
export = plugins;
