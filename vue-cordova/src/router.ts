import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import About from './views/About.vue';
import Profile from './views/Profile.vue';
import Search from './views/Search.vue';
import ReportAdd from './views/ReportAdd.vue';
import ReportEdit from './views/ReportEdit.vue';
import ReportList from './views/ReportList.vue';
import ReportServer from './views/ReportServer.vue';
import ReportView from './views/ReportView.vue';

Vue.use(Router);


export default new Router({
	mode: 'history',
	base: '/',
	routes: [{
		path: '/home/index',
		name: 'home',
		component: Home
	},{
		path: '/',
		component: Home
	},
	{
		path: '/home/about',
		name: 'about',
		component: About
	},
	{
		path: '/home/profile',
		name: 'profile',
		component: Profile
	},
	{
		path: '/home/search',
		name: 'search',
		component: Search
	},
	{
		path: '/report/add',
		name: 'reportAdd',
		component: ReportAdd
	},
	{
		path: '/report/server',
		name: 'reportServer',
		component: ReportServer
	},
	{
		path: '/report/view',
		name: 'reportView',
		component: ReportView
	},
	{
		path: '/report/edit',
		name: 'reportEdit',
		component: ReportEdit
	},
	{
		path: '/report/list',
		name: 'reportList',
		component: ReportList
	}]
});
