export interface Comment {
	id: string;
	text: string;
	accountId: string;
	account: string;
	reportId: string;
}
