export interface Account {
	id: string;
	fbId: string;
	name: string;
}
