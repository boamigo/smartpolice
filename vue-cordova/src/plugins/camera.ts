/// <reference path="../../import/cordova-plugin-crop.d.ts" />
export class Camera {

	takePicture(): Promise<string> {
		return new Promise<string>((resolve, reject) => {
			let camera = (navigator as any).camera;
			camera.getPicture(
				(uri: string) => resolve(uri),
				(err: any) => reject(err),
				{
					destinationType: camera.DestinationType.FILE_URI,
					mediaType: camera.MediaType.PICTURE,
					sourceType: camera.PictureSourceType.CAMERA
				});
		});
	}

	fromGallery(): Promise<string> {
		return new Promise<string>((resolve, reject) => {
			let camera = (navigator as any).camera;
			camera.getPicture(
				(uri: string) => resolve(uri),
				(err: any) => reject(err),
				{
					destinationType: camera.DestinationType.FILE_URI,
					mediaType: camera.MediaType.PICTURE,
					sourceType: camera.PictureSourceType.PHOTOLIBRARY
				});
		});
	}

	cropImage(uri: string): Promise<string> {
		return new Promise<string>((resolve, reject) => {
			(window as any).plugins.crop(
				(newUri: string) => resolve(newUri),
				(err: any) => reject(err),
				uri,
				{ keepingAspectRatio: false });
		});
	}
}
