declare const cordova: any;

export class Clipboard {

	copy(text: string): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			(cordova as any).plugins.clipboard.copy(
				text,
				() => resolve(),
				(err: any) => reject(err));
		});
	}
}
