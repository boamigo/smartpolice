export class Viber {

	shareText(text: string): Promise<string> {
		return new Promise<string>((resolve, reject) => {
			(window as any).viber.shareText(text,
				(res: any) => resolve(res),
				(err: any) => reject(err));
		});
	}

	shareImages(pathList: string[]): Promise<string> {
		return new Promise<string>((resolve, reject) => {
			(window as any).plugins.viber.shareImages(pathList,
				(res: any) => resolve(res),
				(err: any) => reject(err));
		});
	}
}
