export class Toast {
	showShortTop(msg: string): void {
		(window as any).plugins.toast.showShortBottom(msg);
	}

	showLongBottom(msg: string): void {
		(window as any).plugins.toast.showLongBottom(msg);
	}
}
