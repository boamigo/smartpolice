/// <reference path="../../import/cordova-plugin-file.d.ts" />

declare const cordova: any;
export class File {

	getDirectory(path: string): Promise<DirectoryEntry> {
		return new Promise<DirectoryEntry>((resolve, reject) => {
			window.resolveLocalFileSystemURL(
				path,
				entry => resolve(<DirectoryEntry>entry),
				err => reject('File Permission Denied'));
		});
	}

	getFile(path: string): Promise<FileEntry> {
		return new Promise<FileEntry>((resolve, reject) => {
			window.resolveLocalFileSystemURL(
				path,
				entry => resolve(<FileEntry>entry),
				err => reject('File Permission Denied'));
		});
	}

	getFileInDir(folder: DirectoryEntry, name: string): Promise<FileEntry> {
		return new Promise<FileEntry>((resolve, reject) => {
			folder.getFile(
				name,
				{ create: true, exclusive: false },
				entry => resolve(<FileEntry>entry),
				err => reject('File Permission Denied'));
		});
	}

	readJson(entry: FileEntry): Promise<any> {
		return new Promise<any>((resolve, reject) => {
			entry.file(blob => {
				let reader = new FileReader();
				reader.onloadend = () => {
					let data = reader.result ? JSON.parse(<string>reader.result) : null;
					resolve(data);
				};
				reader.readAsText(blob);
			},
			error => reject('Failed reading from file: ' + error));
		});
	}

	writeJson(entry: FileEntry, data: any): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			entry.createWriter(writer => {
				writer.onwriteend = () => resolve();
				writer.onerror = () => reject('Failed writing to file');
				let blob = new Blob([JSON.stringify(data,)], {type : 'application/json'});
				writer.write(blob);
			});
		});
	}

	deleteFile(file: Entry): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			file.remove(
				() => resolve(),
				err => reject(err));
		});
	}

	getRootDirectory(): Promise<DirectoryEntry> {
		return new Promise<DirectoryEntry>((resolve, reject) => {
			window.requestFileSystem(
				LocalFileSystem.PERSISTENT,
				0,
				fs => resolve(fs.root),
				err => reject('Permission Denied'));
		});
	}

	getAppFolder(root: DirectoryEntry): Promise<DirectoryEntry> {
		return new Promise<DirectoryEntry>((resolve, reject) => {
			root.getDirectory(
				'smartpolice',
				{ create: true, exclusive: false },
				entry => resolve(entry),
				err => reject('Create Directory Fail'));
		});
	}

	moveFile(file: Entry, newName: string, dest: DirectoryEntry): Promise<Entry> {
		return new Promise<Entry>((resolve, reject) => {
			file.moveTo(
				dest,
				newName,
				entry => resolve(entry),
				err => reject(err));
		});
	}

	moveFileToAppFolder(file: Entry, newName: string): Promise<Entry> {
		return this.getRootDirectory()
			.then(root => this.getAppFolder(root))
			.then(appFolder => this.moveFile(file, newName, appFolder));
	}

	getExternalPath(entry: Entry): string {
		return (cordova as any).file.externalRootDirectory + entry.fullPath.substr(1);
	}
}
