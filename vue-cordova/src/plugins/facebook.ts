export class Facebook {

	login(): Promise<any> {
		return new Promise<any>((resolve, reject) => {
			(window as any).facebookConnectPlugin.login(
				['public_profile'],
				(res: any) => resolve(res),
				(err: any) => reject(err));
		});
	}

	logout(): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			(window as any).facebookConnectPlugin.logout(
				(res: any) => resolve(),
				(err: any) => reject(err));
		});
	}

	getLoginStatus(): Promise<any> {
		return new Promise<any>((resolve, reject) => {
			(window as any).facebookConnectPlugin.getLoginStatus(
				(res: any) => resolve(res),
				(err : any)=> reject(err));
		});
	}

	getName(): Promise<any> {
		return new Promise<any>((resolve, reject) => {
			(window as any).facebookConnectPlugin.api(
				'/me?fields=id,name',
				['public_profile'],
				(res: any) => resolve(res),
				(err: any) => reject(err));
		});
	}
}
