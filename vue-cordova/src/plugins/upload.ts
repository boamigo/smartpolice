/// <reference path="../../import/cordova-plugin-file-transfer.d.ts" />
export class Upload {
	private host = 'http://smartpolice.azurewebsites.net';

	uploadImage(path: string, reportId: string, carId: string): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			let options: FileUploadOptions = {
				fileKey: 'file',
				
				fileName: carId + '.jpg',
				mimeType: 'image/jpg',
				httpMethod: 'POST',
				params: { reportId: reportId, carId: carId }
			};
			let transfer = new FileTransfer();
			transfer.upload(
				path,
				encodeURI(this.host + '/api/report/upload'),
				response => resolve(),
				err => reject('File Upload Failed'),
				options);
		});
	}
}
