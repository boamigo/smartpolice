import Vue from 'vue';
import VueI18n from 'vue-i18n';

import en from './i18n/en';
import ro from './i18n/ro';
import ru from './i18n/ru';

Vue.use(VueI18n);
export default new VueI18n({
	locale: 'en',
	messages: {
		en: en,
		ro: ro,
		ru: ru
	}
});
