import { AxiosStatic } from 'axios';
import { Toast } from '../plugins/toast';


export class Rest {
	private root = 'http://smartpolice.azurewebsites.net';
	constructor(
		private axios: AxiosStatic,
		private toast: Toast) {
	}

	getRoot(): string {
		return this.root;
	}

	getLocal<T>(url: string): Promise<T> {
		return this.axios.get(url)
			.then(response => <T>response.data);
	}

	get<T>(url: string): Promise<T> {
		return this.axios.get(this.root + url)
			.then(response => <T>response.data)
			.catch((err: any) => {
				if (err.status === -1) {
					let msg = 'nointernet'; //this.$translate.instant('nointernet');
					this.toast.showShortTop(msg);
				}
				else if (err.status === 403) {
					let msg = 'noserver'; //this.$translate.instant('noserver');
					this.toast.showShortTop(msg);
				}

				throw err.status;
			});
	}

	post<T>(url: string, data: any): Promise<T> {
		return this.axios.post(this.root + url, data)
			.then(response => <T>response.data)
			.catch((err: any) => {
				if (err.status === -1) {
					let msg = 'nointernet'; //his.$translate.instant('nointernet');
					this.toast.showShortTop(msg);
				}
				else if (err.status === 403) {
					let msg = 'noserver'; //this.$translate.instant('noserver');
					this.toast.showShortTop(msg);
				}

				throw err.status;
			});
	}
}
