import store from '@/store';
import { Car } from '@/dto/Car';


export class StoreHelper {

	get showLangDialog() {
		return store.state.showLangDialog;
	}
	set showLangDialog(show: boolean) {
		store.commit('setShowLangDialog', show);
	}

	get showBackdrop() {
		return store.state.backdrop;
	}
	set showBackdrop(show: boolean) {
		store.commit('setBackdrop', show);
	}

	carDialogCar!: Car;
	carDialogImageSrc!: string;
	carDialogResolve!: any;

	get showCarDialog() {
		return store.state.showCarDialog;
	}
	set showCarDialog(show: boolean) {
		store.commit('setShowCarDialog', show);
	}

	openCarDialog(car: Car, imageSrc: string): Promise<Car> {
		this.carDialogCar = car;
		this.carDialogImageSrc = imageSrc;
		store.commit('setShowCarDialog', true);
		return new Promise(resolve => {
			this.carDialogResolve = resolve;
		});
	}
}
