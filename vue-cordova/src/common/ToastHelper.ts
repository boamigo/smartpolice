import { Toast } from '../plugins/toast';
import { Sitemap } from '@/common/Sitemap';


export class ToastHelper {
	static $inject = ['$translate', 'Toast'];
	constructor(
		private toast: Toast,
		private sitemap: Sitemap) {
	}

	notifyReportSaved(): void {
		let msg = this.sitemap.translate('reportsaved');
		this.toast.showLongBottom(msg);
	}

	notifyReportDeleted(): void {
		let msg = this.sitemap.translate('reportdeleted');
		this.toast.showShortTop(msg);
	}

	notifyReportPublished(): void {
		let msg = this.sitemap.translate('reportpublished');
		this.toast.showLongBottom(msg);
	}

	notifyIsPublished(): void {
		let msg = this.sitemap.translate('ispublished');
		this.toast.showLongBottom(msg);
	}

	notifyAccountSaved(): void {
		let msg = this.sitemap.translate('account.created');
		this.toast.showLongBottom(msg);
	}

	notifyAccountDuplicate(): void {
		let msg = this.sitemap.translate('username.exists');
		this.toast.showLongBottom(msg);
	}

	notifyFacebookProblem(): void {
		let msg = this.sitemap.translate('facebook.problem');
		this.toast.showLongBottom(msg);
	}

	notifyCopyToClipboard(): void {
		let msg = this.sitemap.translate('msgcopy');
		this.toast.showLongBottom(msg);
	}
}
