export interface Model<T> {
	$model: T;
	$invalid: boolean;
	$dirty: boolean;
	$anyDirty: boolean;
	$error: boolean;
	$anyError: boolean;
	$pending: boolean;
}
