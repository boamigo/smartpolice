import { Account } from '../dto/Account';
import { Street } from '../dto/Street';
import * as moment from 'moment';


export class AppHelper {

	setUser(user: Account): void {
		localStorage.setItem('CUR_USER', JSON.stringify(user));
	}

	clearUser(): void {
		localStorage.removeItem('CUR_USER');
	}

	getUser(): Account {
		let text = localStorage.getItem('CUR_USER');
		if (!text) {
			return null!;
		}

		return JSON.parse(text);
	}

	formatDate(value: Date): string {
		return (<any>moment)(value).format('DD/MM/YYYY');
	}

	parseDate(value: string): Date {
		return (<any>moment)(value, 'DD/MM/YYYY').toDate();;
	}

	formatCurrentTime(): string {
		return (<any>moment)().format('HH:mm');
	}

	getNewImageFileName(): string {
		return (<any>moment)().format('YYYYMMDD_HHmmss.jpg');
	}

	filterStreets(q: string, streets: Street[]): Street[] {
		q = q.toLowerCase();
		let priority1: Street[] = [];
		let priority2: Street[] = [];
		for(let item of streets) {
			let text = item.name.toLowerCase();
			let added = false;
			let parts = text.split(' ');
			for(let part of parts) {
				if (part.indexOf(q) === 0) {
					priority1.push(item);
					added = true;
					break;
				}
			}

			if (added) {
				continue;
			}

			if (text.indexOf(q) !== -1) {
				priority2.push(item);
			}
		}

		priority1 = priority1.sort((a, b) => a.pop - b.pop);
		let filtered: Street[] = [];
		if (priority1.length > 0) {
			for(let item of priority1) {
				if (filtered.length >= 6) {
					break;
				}

				filtered.push(item);
			}
		}

		if (filtered.length < 6 && priority2.length > 0) {
			priority2 = priority2.sort((a, b) => a.pop - b.pop);
			for(let item of priority2) {
				if (filtered.length >= 6) {
					break;
				}

				filtered.push(item);
			}
		}

		return filtered;
	}
}
