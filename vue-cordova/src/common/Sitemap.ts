import { Report } from "../dto/Report";
import { Rest } from './Rest';
import { Vue } from 'vue-property-decorator';


export class Sitemap {
	report!: Report;
	appRoot!: Vue;
	pageNum = 1;

	constructor(
		private rest: Rest) {
	}

	setLocale(locale: string): void {
		this.appRoot.$i18n.locale = locale;
		localStorage.setItem('NG_TRANSLATE_LANG_KEY', locale);
	}

	setRootComponent(appRoot: Vue): void {
		this.appRoot = appRoot;
	}

	translate(key: string): string {
		return this.appRoot.$t(key) as string;
	}

	getBackendRoot(): string {
		return this.rest.getRoot();
	}

	getLang(): string {
		return this.appRoot.$i18n.locale;
	}

	getPath(): string {
		return this.appRoot.$route.path;
	}

	queryString(): any {
		return this.appRoot.$route.query;
	}

	getParams(): any {
		return this.appRoot.$route.params;
	}

	gotoHome(): void {
		this.appRoot.$router.push('/home/index');
	}

	gotoSearch(): void {
		this.appRoot.$router.push('/home/search');
	}

	gotoReportAdd(): void {
		this.report = <any>null;
		this.appRoot.$router.push('/report/add');
	}

	gotoReportEdit(item: Report): void {
		this.report = item;
		this.appRoot.$router.push('/report/edit');
	}

	gotoReportView(item: Report): void {
		this.report = item;
		this.appRoot.$router.push('/report/view');
	}

	gotoReportList(): void {
		this.appRoot.$router.push('/report/list');
	}

	gotoReportServer(): void {
		this.appRoot.$router.push('/report/server');
	}

	exit(): void {
		try {
			(<any>navigator).app.exitApp();
		}
		catch(err) {
			alert(err);
			try {
				(<any>navigator).device.exitApp();
			}
			catch(err) {
				alert(err);
				try {
					window.close();
				}
				catch(err) {
					alert(err);
				}
			}
		}
	}
}
