import { Rest } from '../common/Rest';
import { Street } from '@/dto/Street';


export class StreetService {

	constructor(
		private rest: Rest) {
	}

	getStreets(): Promise<Street[]> {
		return this.rest.getLocal<Street[]>('streets.json');
	}
}
