import { File } from '../plugins/file';
import { Rest } from '../common/Rest';
import { Report } from '../dto/Report';
import { Car } from '../dto/Car';
import { Page } from '../dto/Page';
import * as shortid from 'shortid';


export class ReportService {
	static $inject = ['File', 'Rest'];
	constructor(
		private file: File,
		private rest: Rest) {
	}

	getLocalReports(): Promise<Report[]> {
		return this.getReportsFile()
			.then(reportsFile => this.file.readJson(reportsFile))
			.then(json => <Report[]>json);
	}

	saveLocalReport(item: Report): Promise<string> {
		let newId = '';
		return this.getReportsFile()
		.then(reportsFile => this.file.readJson(reportsFile))
		.then(json => {
			let reports: Report[] = json;
			if (!reports) {
				reports = [];
			}

			newId = shortid.generate();
			item.id = newId;
			reports.push(item);
			return reports;
		})
		.then(reports => {
			return this.getReportsFile()
				.then(reportsFile => this.file.writeJson(reportsFile, reports));
		})
		.then(() => newId);
	}

	updateLocalReport(item: Report): Promise<string> {
		return this.getReportsFile()
		.then(reportsFile => this.file.readJson(reportsFile))
		.then(json => {
			let reports: Report[] = json;
			for(let rep of reports) {
				if (rep.id === item.id) {
					rep.address = item.address;
					rep.date = item.date;
					rep.time = item.time;
					rep.notes = item.notes;
					rep.cars = item.cars;
					rep.isPublished = item.isPublished;
					break;
				}
			}

			return reports;
		})
		.then(reports => {
			return this.getReportsFile()
				.then(reportsFile => this.file.writeJson(reportsFile, reports));
		})
		.then(() => item.id);
	}

	deleteLocalReport(item: Report): Promise<void> {
		return this.getReportsFile()
		.then(reportsFile => this.file.readJson(reportsFile))
		.then(json => {
			let reports: Report[] = json;
			if (!reports) {
				reports = [];
			}

			let index = 0;
			let found = false;
			for(let rep of reports) {
				if (rep.id === item.id) {
					found = true;
					break;
				}

				index++;
			}

			if (found) {
				reports.splice(index, 1);
			}

			return reports;
		})
		.then(reports => {
			return this.getReportsFile()
				.then(reportsFile => this.file.writeJson(reportsFile, reports));
		});
	}

	publishReport(data: Report): Promise<Report> {
		return this.rest.post('/api/report/publish', data);
	}

	getServerReports(pageIndex: number): Promise<Page<Report>> {
		return this.rest.get('/api/report/getpage/' + pageIndex);
	}

	searchCar(plate: string): Promise<Car[]> {
		return this.rest.get('/api/report/searchcar/' + plate);
	}

	private getReportsFile(): Promise<FileEntry> {
		return this.file.getRootDirectory()
			.then(root => this.file.getAppFolder(root))
			.then(appFolder => this.file.getFileInDir(appFolder, 'reports.json'));
	}
}
