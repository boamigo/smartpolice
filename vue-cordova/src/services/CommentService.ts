import { Comment } from '../dto/Comment';
import { Rest } from '../common/Rest';

export class CommentService {

	static $inject = ['Rest'];
	constructor(
		private rest: Rest) {
	}

	save(data: Comment): Promise<Comment> {
		return this.rest.post('/api/comment/save', data);
	}

	getByReport(reportId: string): Promise<Comment[]> {
		return this.rest.get('/api/comment/getbyreport/' + reportId);
	}
}
