import { Rest } from '../common/Rest';


export class BrandService {

	constructor(
		private rest: Rest) {
	}

	getBrands(): Promise<string[]> {
		return this.rest.get<string[]>('brands.json');
	}
}
