import { Account } from '../dto/Account';
import { Rest } from '../common/Rest';


export class AccountService {

	constructor(
		private rest: Rest) {
	}

	save(data: Account): Promise<Account> {
		return this.rest.post('/api/account/save', data);
	}

	getByFacebook(facebookId: string): Promise<Account> {
		return this.rest.get('/api/account/getbyfb/' + facebookId);
	}
}
