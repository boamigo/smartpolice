import Vue from 'vue';
import Vuex from 'vuex';


Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		backdrop: false,
		showLangDialog: false,
		showCarDialog: false
	},
	actions: {
	},
	mutations: {
		setShowLangDialog(state: any, value: boolean): void {
			state.showLangDialog = value;
		},
		setBackdrop(state: any, value: boolean): void {
			state.backdrop = value;
		},
		setShowCarDialog(state: any, value: boolean): void {
			state.showCarDialog = value;
		}
	}
});
