import Vue from 'vue';
import App from '@/App.vue';
import router from '@/router';
import BootstrapVue from 'bootstrap-vue';
import VeeValidate from 'vee-validate';
import VueBootstrapTypeahead from 'vue-bootstrap-typeahead';
import i18n from '@/locales';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'font-awesome/css/font-awesome.css';


Vue.config.productionTip = false
Vue.use(BootstrapVue);
Vue.use(VeeValidate);
Vue.component('vue-bootstrap-typeahead', VueBootstrapTypeahead);
i18n.locale = 'en';

new Vue({
	router,
	i18n,
	render: h => h(App)
}).$mount('#app')
