import Vue from 'vue';
import { ElLoadingComponent } from 'element-ui/types/loading';
import VueRouter from 'vue-router';
import { IRouteChangeListener } from '@/common/route-change-listener';


export class Helper {
	loading: ElLoadingComponent = <any>null;
	router: VueRouter = <any>null;
	appComponent: Vue = <any>null;
	routeChangeListener: IRouteChangeListener = <any>null;
	
	init(vue: Vue, routeChangeListener: IRouteChangeListener): void {
		this.appComponent = vue;
		this.router = vue.$router;
		this.routeChangeListener = routeChangeListener;
	}

	fadeIn(): void {
		this.loading = this.appComponent.$loading({
			lock: true,
			text: 'Loading',
			spinner: 'el-icon-loading'
		});
	}

	fadeOut(): void {
		if (this.loading) {
			this.loading.close();
		}
	}

	gotoHome(): void {
		this.router.push('/');
		this.routeChangeListener.onRouteUpdated();
	}

	gotoReportList(pageIndex: number): void {
		this.router.push('/report/list/' + pageIndex);
		this.routeChangeListener.onRouteUpdated();
	}

	gotoReportEdit(reportId: string): void {
		this.router.push('/report/edit/' + reportId);
		this.routeChangeListener.onRouteUpdated();
	}

	getParam(vue: Vue, key: string): string {
		return vue.$route.params[key];
	}

	getParamNum(vue: Vue, key: string): number {
		return vue.$route.params[key] ? Number(vue.$route.params[key]) : <any>null;
	}
}

export default new Helper();
