export interface IRouteChangeListener {

	onRouteUpdated(): void;
}
