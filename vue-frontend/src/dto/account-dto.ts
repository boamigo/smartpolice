export interface AccountDto {
	id: string;
	fbId: string;
	name: string;
}
