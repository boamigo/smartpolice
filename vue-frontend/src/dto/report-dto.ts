import { CarDto } from './car-dto';


export interface ReportDto {
	id: string;
	address: string;
	date: string;
	time: string;
	notes: string;
	cars: CarDto[];
}
