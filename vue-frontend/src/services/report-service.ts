import { AxiosRequestConfig, AxiosStatic } from 'axios';
import { ReportDto } from '@/dto/report-dto';
import { Page } from '@/dto/page';


export class ReportService {

	constructor(private axios: AxiosStatic) {
	}

	getById(id: string): Promise<ReportDto> {
		return this.axios.get('/api/report/getbyid/' + id, this.getConfig())
			.then(response => response.data as ReportDto);
	}

	update(data: ReportDto): Promise<void> {
		return this.axios.post<void>('/api/report/update', data, this.getConfig())
			.then(response => undefined);
	}

	getPage(pageIndex: number): Promise<Page<ReportDto>> {
		return this.axios.get('/api/report/getpage/' + pageIndex)
			.then(response => response.data as Page<ReportDto>);
	}

	delete(reportId: string): Promise<void> {
		return this.axios.delete('/api/report/delete/' + reportId, this.getConfig())
			.then(response => undefined);
	}

	deleteCar(carId: string): Promise<void> {
		return this.axios.delete('/api/report/deletecar/' + carId, this.getConfig())
			.then(response => undefined);
	}

	uploadCar(file: any, reportId: string, carId: string): Promise<void>{
		var fd = new FormData();
		fd.append('file', file);
		fd.append('reportId', reportId);
		fd.append('carId', carId);
		return this.axios.post('api/report/upload', fd, {
			headers: {
				'Content-Type': undefined,
				'x-access-token': localStorage.getItem('x-access-token')
			}
		})
		.then(data => undefined);
	}

	private getConfig(): AxiosRequestConfig {
		let token = localStorage.getItem('x-access-token');
		return {
			headers: {
				'x-access-token': token,
				'Content-Type': 'multipart/form-data'
			}
		};
	}
}
