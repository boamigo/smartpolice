import { AxiosRequestConfig, AxiosStatic } from 'axios';
import { AccountDto } from '../dto/account-dto';
import { Page } from '../dto/page';


export class AccountService {

	constructor(private axios: AxiosStatic) {
	}

	getPage(pageIndex: number): Promise<Page<AccountDto>> {
		return this.axios.get('/api/account/getpage/' + pageIndex, this.getConfig())
		.then(response => response.data as Page<AccountDto>);
	}

	delete(accountId: string): Promise<void> {
		return this.axios.delete('/api/account/delete/' + accountId, this.getConfig())
			.then(response => undefined);
	}

	updateToken(name: string, password: string): Promise<void> {
		return this.axios.post<string>('/token', { name: name, pwd: password })
				.then(response => response.data as string)
				.then(token => localStorage.setItem('x-access-token', token));
	}

	private getConfig(): AxiosRequestConfig {
		let token = localStorage.getItem('x-access-token');
		return {
			headers: {
				'x-access-token': token
			}
		};
	}
}
