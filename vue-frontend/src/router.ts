import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/home.vue';
import ReportList from './views/report/report-list.vue';
import ReportEdit from './views/report/report-edit.vue';
import AccountList from './views/account/account-list.vue';
import Login from './views/login.vue';


Vue.use(Router);

export default new Router({
	routes: [{
		path: '/',
		name: 'home',
		component: Home
	},
	{
		path: '/report/list/:pageIndex',
		name: 'reportList',
		component: ReportList
	},
	{
		path: '/report/edit/:reportId',
		name: 'reportEdit',
		component: ReportEdit
	},
	{
		path: '/account/list/:pageIndex',
		name: 'accountList',
		component: AccountList
	},
	{
		path: '/login',
		name: 'login',
		component: Login
	}]
});
