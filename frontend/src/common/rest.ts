import * as angular from 'angular';

export class Rest {
	static $inject = ['$http', '$q'];
	constructor(
		private $http: angular.IHttpService,
		private $q: angular.IQService) {
	}

	get<T>(url: string): angular.IPromise<T> {
		let config: angular.IRequestShortcutConfig = {
			headers: {
				'x-access-token': localStorage.getItem('x-access-token')
			}
		};
		return this.$http.get(url, config)
			.then(response => <T>response.data);
	}

	post<T>(url: string, data: any): angular.IPromise<T> {
		let config: angular.IRequestShortcutConfig = {
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': localStorage.getItem('x-access-token')
			}
		};
		return this.$http.post(url, data, config)
			.then(response => <T>response.data);
	}

	put<T>(url: string, data: any): angular.IPromise<T> {
		let config: angular.IRequestShortcutConfig = {
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': localStorage.getItem('x-access-token')
			}
		};
		return this.$http.put(url, data, config)
			.then(response => <T>response.data);
	}

	delete<T>(url: string): angular.IPromise<T> {
		let config: angular.IRequestShortcutConfig = {
			headers: {
				'x-access-token': localStorage.getItem('x-access-token')
			}
		};
		return this.$http.delete(url, config)
			.then(response => <T>response.data);
	}

	upload(file: string, url: string, data: any = null): angular.IPromise<void>{
		var fd = new FormData();
		fd.append('file', file);
		if (data) {
			for(let key in data) {
				fd.append(key, data[key]);
			}
		}

		return this.$http.post(url, fd, {
			transformRequest: angular.identity,
			headers: {
				'Content-Type': undefined,
				'x-access-token': localStorage.getItem('x-access-token')
			}
		})
		.then(data => null);
	}
}
