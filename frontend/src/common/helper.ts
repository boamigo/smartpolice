export class Helper {

	static $inject = ['$location', '$q'];
	constructor(
		private $location: angular.ILocationService,
		private $q: angular.IQService) {
	}

	onRouteChange(scope: angular.IScope, listener: () => any): void {
		scope.$on('$routeChangeSuccess', listener());
	}

	resolve<T>(): angular.IPromise<T> {
		return this.$q.resolve<T>(null);
	}

	fadeIn(): void {
		window.document.getElementById('modalBackdrop').style.display = 'block';
	}

	fadeOut(): void {
		window.document.getElementById('modalBackdrop').style.display = 'none';
	}

	queryString(): any {
		return this.$location.search();
	}

	gotoHome(): void {
		this.$location.path('home');
	}

	gotoReportList(pageIndex: number): void {
		this.$location.path('report/list/' + pageIndex);
	}

	gotoCarList(reportId: string): void {
		this.$location.path('/car/list/' + reportId);
	}
}
