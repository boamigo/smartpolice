import { Rest } from '../common/rest';
import { ReportDto } from '../dto/report-dto';
import { Page } from '../dto/page';

export class ReportService {

	static $inject = ['rest'];
	constructor(private rest: Rest) {
	}

	getById(id: string): angular.IPromise<ReportDto> {
		return this.rest.get('/api/report/getbyid/' + id);
	}

	update(data: ReportDto): angular.IPromise<void> {
		return this.rest.post('/api/report/update', data);
	}

	getPage(pageIndex: number): angular.IPromise<Page<ReportDto>> {
		return this.rest.get('/api/report/getpage/' + pageIndex);
	}

	delete(reportId: string): angular.IPromise<void> {
		return this.rest.delete('/api/report/delete/' + reportId);
	}

	deleteCar(carId: string): angular.IPromise<void> {
		return this.rest.delete('/api/report/deletecar/' + carId);
	}

	uploadCar(file: string, reportId: string, carId: string): angular.IPromise<void> {
		return this.rest.upload(file, 'api/report/upload', { reportId: reportId, carId: carId });
	}
}
