import { Rest } from '../common/rest';
import { AccountDto } from '../dto/account-dto';
import { Page } from '../dto/page';

export class AccountService {

	static $inject = ['rest'];
	constructor(private rest: Rest) {
	}

	getPage(pageIndex: number): angular.IPromise<Page<AccountDto>> {
		return this.rest.get('/api/account/getpage/' + pageIndex);
	}

	delete(accountId: string): angular.IPromise<void> {
		return this.rest.delete('/api/account/delete/' + accountId);
	}
}
