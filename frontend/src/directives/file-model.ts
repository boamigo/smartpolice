export class FileModel implements angular.IDirective {
	restrict = 'A';

	static $inject = ['$parse'];
	constructor (
		private $parse: angular.IParseService) {
	}

	link(scope: angular.IScope, element: any, attrs: angular.IAttributes): void {
		let model = this.$parse(attrs.fileModel);
		let modelSetter = model.assign;
		element.bind('change', () => {
			scope.$apply(() => {
				modelSetter(scope, element[0].files[0]);
			});
		});
	}
}
