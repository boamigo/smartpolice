import * as angular from 'angular';

import { FileModel } from './directives/file-model';
import { LoginController } from './controllers/login-ctrl';
import { ReportListController } from './controllers/report-list-ctrl';
import { ReportEditController } from './controllers/report-edit-ctrl';
import { CarDialogController } from './controllers/car-dialog-ctrl';
import { AccountListController } from './controllers/account-list-ctrl';

import { Helper } from './common/helper';
import { Rest } from './common/rest';
import { ReportService } from './services/report-service';
import { AccountService } from './services/account-service';

export class Application {
	private static appModule : angular.IModule;
	public static create() {
		Application.appModule = angular.module('vkusno', ['ngRoute', 'ui.bootstrap']);
		Application.appModule.config(['$routeProvider', '$locationProvider', ($routeProvider: angular.route.IRouteProvider, $locationProvider: angular.ILocationProvider) => {
			$locationProvider.html5Mode({ enabled:true });
			$routeProvider
			.when('/', {
				templateUrl: 'views/home/index.html'
			})
			.when('/login', {
				templateUrl: '/views/home/login.html',
				controller: 'LoginController',
				controllerAs: 'ctrl'
			})
			.when('/report/list/:pageIndex', {
				templateUrl: '/views/report/list.html',
				controller: 'ReportListController',
				controllerAs: 'ctrl'
			})
			.when('/report/edit/:reportId', {
				templateUrl: '/views/report/edit.html',
				controller: 'ReportEditController',
				controllerAs: 'ctrl'
			})
			.when('/account/list/:pageIndex', {
				templateUrl: '/views/account/list.html',
				controller: 'AccountListController',
				controllerAs: 'ctrl'
			});
		}]);
	}

	public static service(name: string, serviceConstructor: angular.Injectable<Function>) {
		Application.appModule.service(name, serviceConstructor);
	}

	public static controller(name: string, controllerConstructor: angular.Injectable<angular.IControllerConstructor>) {
		Application.appModule.controller(name, controllerConstructor);
	}

	public static filter(name: string, filterFactoryFunction: angular.Injectable<Function>) {
		Application.appModule.filter(name, filterFactoryFunction);
	}

	public static directive(name: string, directiveConstructor: angular.Injectable<any>) {
		Application.appModule.directive(name, directiveConstructor);
	}
}

(():void => {
	Application.create();

	Application.directive('fileModel', FileModel);

	Application.controller("LoginController", LoginController);
	Application.controller("ReportListController", ReportListController);
	Application.controller("ReportEditController", ReportEditController);
	Application.controller("CarDialogController", CarDialogController);
	Application.controller("AccountListController", AccountListController);

	Application.service('Helper', Helper);
	Application.service('rest', Rest);
	Application.service('ReportService', ReportService);
	Application.service('AccountService', AccountService);
})();
