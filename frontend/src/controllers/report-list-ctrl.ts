import { ReportDto } from '../dto/report-dto';
import { Helper } from '../common/helper';
import { ReportService } from '../services/report-service';

export class ReportListController {
	pageIndex: number;
	pageNum: number;
	reports: ReportDto[] = null;
	total: number;

	static $inject = ['$scope', 'Helper', '$routeParams', 'ReportService'];
	constructor (
		private $scope: angular.IScope,
		private helper: Helper,
		private $routeParams: angular.route.IRouteParamsService,
		private reportService: ReportService) {
		this.helper.onRouteChange(this.$scope, () => this.init());
	}

	init(): void {
		this.helper.fadeIn();
		this.pageIndex = this.$routeParams['pageIndex'] || 0;
		this.pageNum = Number(this.pageIndex) + 1;
		this.loadRows()
			.finally(() => this.helper.fadeOut());
	}

	loadRows(): angular.IPromise<void> {
		return this.reportService.getPage(this.pageIndex)
		.then(page => {
			this.reports = page.items;
			this.total = page.total;
		});
	}

	onDelete(id: string): void {
		this.helper.fadeIn();
		this.reportService.delete(id)
			.then(() => this.loadRows())
			.finally(() => this.helper.fadeOut());
	}

	onPage(aa: any): void {
		this.helper.fadeIn();
		this.pageIndex = this.pageNum - 1;
		this.loadRows()
			.finally(() => this.helper.fadeOut());
	}
}
