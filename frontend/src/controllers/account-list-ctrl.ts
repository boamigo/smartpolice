import { ReportDto } from '../dto/report-dto';
import { AccountDto } from '../dto/account-dto';
import { Helper } from '../common/helper';
import { AccountService } from '../services/account-service';

export class AccountListController {
	pageIndex: number;
	pageNum: number;
	accounts: AccountDto[] = null;
	total: number;

	static $inject = ['$scope', 'Helper', '$routeParams', 'AccountService'];
	constructor (
		private $scope: angular.IScope,
		private helper: Helper,
		private $routeParams: angular.route.IRouteParamsService,
		private accountService: AccountService) {
		this.helper.onRouteChange(this.$scope, () => this.init());
	}

	init(): void {
		this.helper.fadeIn();
		this.pageIndex = this.$routeParams['pageIndex'] || 0;
		this.pageNum = Number(this.pageIndex) + 1;
		this.loadRows()
			.finally(() => this.helper.fadeOut());
	}

	loadRows(): angular.IPromise<void> {
		return this.accountService.getPage(this.pageIndex)
		.then(page => {
			this.accounts = page.items;
			this.total = page.total;
		});
	}

	onDelete(id: string): void {
		this.helper.fadeIn();
		this.accountService.delete(id)
			.then(() => this.loadRows())
			.finally(() => this.helper.fadeOut());
	}

	onPage(aa: any): void {
		this.helper.fadeIn();
		this.pageIndex = this.pageNum - 1;
		this.loadRows()
			.finally(() => this.helper.fadeOut());
	}
}
