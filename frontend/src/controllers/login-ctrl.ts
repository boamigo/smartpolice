import { Rest } from '../common/rest';
import { Helper } from '../common/helper';

export class LoginController {
	name: string;
	pwd: string;

	static $inject = ['rest', 'Helper'];
	constructor (
		private rest: Rest,
		private helper: Helper) {
	}

	onSubmit(): void {
		this.helper.fadeIn();
		this.rest.post<string>('/token', { name: this.name, pwd: this.pwd })
			.then(token => localStorage.setItem('x-access-token', token))
			.then(() => this.helper.gotoHome())
			.finally(() => this.helper.fadeOut());
	}
}
