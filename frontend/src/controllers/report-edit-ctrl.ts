import { ReportDto } from '../dto/report-dto';
import { CarDto } from '../dto/car-dto';
import { Helper } from '../common/helper';
import { ReportService } from '../services/report-service';

export class ReportEditController {
	reportId: string;
	address: string;
	date: string;
	time: string;
	notes: string;
	cars: CarDto[];

	static $inject = ['$scope', 'Helper', '$routeParams', 'ReportService', '$uibModal'];
	constructor (
		private $scope: angular.IScope,
		private helper: Helper,
		private $routeParams: angular.route.IRouteParamsService,
		private reportService: ReportService,
		private $uibModal: any) {
		this.helper.onRouteChange(this.$scope, () => this.init());
	}

	init(): void {
		this.helper.fadeIn();
		this.reportId = this.$routeParams['reportId'];
		this.load()
		.finally(() => this.helper.fadeOut());
	}

	onSave(): void {
		this.helper.fadeIn();
		let data: ReportDto = {
			id: this.reportId,
			address: this.address,
			date: this.date,
			time: this.time,
			notes: this.notes,
			cars: []
		};
		for(let car of this.cars) {
			data.cars.push(car);
		}

		this.reportService.update(data)
			.then(() => this.helper.gotoReportList(0))
			.finally(() => this.helper.fadeOut());
	}

	onCancel(): void {
		this.helper.gotoReportList(0);
	}

	onDeleteCar(car: CarDto): void {
		this.helper.fadeIn();
		this.reportService.deleteCar(car.id)
			.then(() => this.load())
			.finally(() => this.helper.fadeOut());
	}

	onUploadCar(car: CarDto): void {
		if (!car.image) {
			return;
		}

		this.helper.fadeIn();
		this.reportService.uploadCar(car.image, this.reportId, car.id)
			.then(() => this.load())
			.finally(() => this.helper.fadeOut());
	}

	onCarZoom(car: CarDto): void {
		let dialog = this.$uibModal.open({
			animation: true,
			templateUrl: '/views/report/car-dialog.html',
			controller: 'CarDialogController',
			controllerAs: 'ctrl',
			resolve: { car: () => car, reportId: () => this.reportId }
		});
	}

	private load(): angular.IPromise<void> {
		return this.reportService.getById(this.reportId)
		.then(data => {
			this.address = data.address;
			this.date = data.date;
			this.time = data.time;
			this.notes = data.notes;
			this.cars = data.cars;
		});
	}
}
