const gulp = require('gulp');
const del = require('del');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const tsProject = ts.createProject('tsconfig.json', {
	typescript: require('typescript') 
});
const browserify = require('browserify');
const source = require('vinyl-source-stream');

gulp.task('clean', (cb) => {
	return del(['src/**/*.js'], cb);
});

gulp.task('compile', ['clean'], () => {
	var tsResult = gulp.src('src/**/*.ts')
		.pipe(tsProject());
	return tsResult.js
		.pipe(gulp.dest('src'));
});

gulp.task('browserify', ['compile'], () => {
	return browserify()
		.add('src/app.js')
		.exclude('angular')
		.ignore('angular')
		.bundle()
		.pipe(source('bundle.js'))
		.pipe(gulp.dest('src'));
});

gulp.task('content', ['browserify'], () => {
	return gulp.src(['src/**/*.html', 'src/bundle.js'])
		.pipe(gulp.dest('../backend/src/www'));
});

gulp.task('libs', ['content'], () => {
	return gulp.src([
		'angular/angular.min.js',
		'angular-route/angular-route.min.js',
		'angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
		'angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
		'bootstrap/dist/css/bootstrap.css',
		'bootstrap/dist/fonts/**'
	],
	{ cwd: 'node_modules/**' })
	.pipe(gulp.dest('../backend/src/www/libs'));
});

gulp.task('deploy_content', ['browserify'], () => {
	return gulp.src(['src/**/*.html', 'src/bundle.js'])
		.pipe(gulp.dest('../backend/deploy/src/www'));
});

gulp.task('deploy_libs', ['deploy_content'], () => {
	return gulp.src([
		'angular/angular.min.js',
		'angular-route/angular-route.min.js',
		'angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
		'angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
		'bootstrap/dist/css/bootstrap.css',
		'bootstrap/dist/fonts/**'
	],
	{ cwd: 'node_modules/**' })
	.pipe(gulp.dest('../backend/deploy/src/www/libs'));
});
