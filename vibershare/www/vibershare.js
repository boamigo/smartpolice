var exec = require('cordova/exec');
var plugin = {
	shareText: function(arg0, success, error) {
		exec(success, error, 'vibershare', 'shareText', [arg0]);
	},
	shareImages:  function(arg0, success, error) {
		exec(success, error, 'vibershare', 'shareImages', arg0);
	}
};

module.exports = plugin;