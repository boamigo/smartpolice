package com.jeneva.cordova;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import org.apache.cordova.CallbackContext;

import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ViberSharePlugin extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("shareText")) {
            String message = args.getString(0);
            this.shareText(message, callbackContext);
            return true;
        }
        else if (action.equals("shareImages")) {
            this.shareImages(args, callbackContext);
            return true;
        }

        return false;
    }

    private void shareText(String text, CallbackContext callbackContext) {
        if (text == null && text.length() == 0) {
            callbackContext.error("empty text not permited");
        }

        try {
            Context context = this.cordova.getContext();
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, text);
            List<ResolveInfo> packages = context.getPackageManager().queryIntentActivities(shareIntent, 0);
            for (ResolveInfo packageInfo : packages) {
                String packageName = packageInfo.activityInfo.packageName.toLowerCase(Locale.getDefault());
                String name = packageInfo.activityInfo.name.toLowerCase(Locale.getDefault());
                if (packageName.contains("com.viber.voip") || name.contains("com.viber.voip")) {
                    shareIntent.setPackage(packageInfo.activityInfo.packageName);
                    context.startActivity(Intent.createChooser(shareIntent, "Share..."));
                    break;
                }
            }
            callbackContext.success();
        }
        catch (Exception ex) {
            callbackContext.error(ex.getMessage());
        }
    }

    private void shareImages(JSONArray pathList, CallbackContext callbackContext) {
        if (pathList == null || pathList.length() == 0) {
            callbackContext.error("empty array not permited");
        }

        try {
            Context context = this.cordova.getContext();
            Intent shareIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            shareIntent.setType("image/jpg");
            ArrayList<Uri> files = new ArrayList<Uri>();
            int len = pathList.length();
            for (int i = 0; i < len; i++) {
                files.add(Uri.parse(pathList.getString(i)));
            }

            shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
            List<ResolveInfo> packages = context.getPackageManager().queryIntentActivities(shareIntent, 0);
            for (ResolveInfo packageInfo : packages) {
                String packageName = packageInfo.activityInfo.packageName.toLowerCase(Locale.getDefault());
                String name = packageInfo.activityInfo.name.toLowerCase(Locale.getDefault());
                if (packageName.contains("com.viber.voip") || name.contains("com.viber.voip")) {
                    shareIntent.setPackage(packageInfo.activityInfo.packageName);
                    context.startActivity(Intent.createChooser(shareIntent, "Share..."));
                    break;
                }
            }

            callbackContext.success();
        }
        catch (Exception ex) {
            callbackContext.error(ex.getMessage());
        }
    }
}
