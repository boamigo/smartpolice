export interface Report {
	_id: string;
	address: string;
	notes: string;
	date: Date;
	published: Date;
	show: boolean;
}
