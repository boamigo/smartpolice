export interface Car {
	_id: string;
	plate: string;
	brand: string;
	reportId: string;
}
