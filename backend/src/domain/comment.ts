export interface Comment {
	_id: string;
	text: string;
	accountId: string;
	reportId: string;
	index: number;
}
