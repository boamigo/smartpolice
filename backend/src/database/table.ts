import * as Nedb from 'nedb';

export class Table<T> {

	static PAGE_SIZE = 12;
	private file: string;
	private source: Nedb = null;

	public constructor(file: string) {
		this.file = file;
	}

	public load(): Promise<void> {
		if (this.source) {
			return;
		}

		return new Promise<void>((resolve, reject) => {
			this.source = new Nedb({ filename: this.file });
			this.source.loadDatabase(err => {
				err ? reject() : resolve();
			});
		});
	}

	public count(query: any): Promise<number> {
		return new Promise<number>((resolve, reject) => {
			this.source.count(query, (err, n) => {
				err ? reject() : resolve(n);
			});
		});
	}

	public find(query: any, sort?: any): Promise<T[]> {
		return new Promise<T[]>((resolve, reject) => {
			let cursor = this.source.find<T>(query);
			if (sort) {
				cursor = cursor.sort(sort);
			}

			cursor.exec((err, items) => {
				err ? reject() : resolve(items);
			});
		});
	}

	public findPaged(query: any, pageIndex: number, sort?: any): Promise<T[]> {
		return new Promise<T[]>((resolve, reject) => {
			let cursor = this.source.find<T>(query);
			if (sort) {
				cursor = cursor.sort(sort);
			}

			cursor
				.skip(Table.PAGE_SIZE * pageIndex)
				.limit(Table.PAGE_SIZE)
				.exec((err, items) => {
					err ? reject() : resolve(items);
				});
		});
	}

	public findOne(query: any): Promise<T> {
		return new Promise<T>((resolve, reject) => {
			this.source.findOne<T>(query, (err, items) => {
				err ? reject() : resolve(items);
			});
		});
	}

	public findById(id: string): Promise<T> {
		return new Promise<T>((resolve, reject) => {
			this.source.findOne<T>({ _id: id}, (err, item) => {
				err ? reject() : resolve(item);
			});
		});
	}

	public insert(newItem: T): Promise<T> {
		return new Promise<T>((resolve, reject) => {
			this.source.insert<T>(newItem, (err, item) => {
				err ? reject() : resolve(item);
			});
		});
	}

	public update(newItem: T): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			this.source.update<T>({ _id: newItem['_id'] }, newItem, {}, (err, item) => {
				err ? reject() : resolve();
			});
		});
	}

	public updateBy(query: any, updateQuery: any): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			this.source.update(query, updateQuery, { multi: true }, (err, item) => {
				err ? reject() : resolve();
			});
		});
	}

	public delete(id: string): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			this.source.remove({ _id: id}, (err, item) => {
				err ? reject() : resolve();
			});
		});
	}

	public deleteBy(query: any): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			this.source.remove(query, { multi: true }, (err, item) => {
				err ? reject() : resolve();
			});
		});
	}
}
