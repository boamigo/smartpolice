import { Table } from './table';
import { Report } from '../domain/report';
import { Car } from '../domain/car';
import { Account } from '../domain/account';
import { Comment } from '../domain/comment';

export class Database {

	private carTable: Table<Car> = null;
	private reportTable: Table<Report> = null;
	private accountTable: Table<Account> = null;
	private commentTable: Table<Comment> = null;

	constructor() {
	}

	public car(): Table<Car> {
		if(!this.carTable) {
			this.carTable = new Table<Car>('data/car.db');
		}

		return this.carTable;
	}

	public report(): Table<Report> {
		if(!this.reportTable) {
			this.reportTable = new Table<Report>('data/report.db');
		}

		return this.reportTable;
	}

	public account(): Table<Account> {
		if(!this.accountTable) {
			this.accountTable = new Table<Account>('data/account.db');
		}

		return this.accountTable;
	}

	public comment(): Table<Comment> {
		if(!this.commentTable) {
			this.commentTable = new Table<Comment>('data/comment.db');
		}

		return this.commentTable;
	}
}
