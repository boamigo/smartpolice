import { Car } from '../domain/car';
import { Page } from '../dto/page';
import { Database } from '../database/database';

export class CarDao {

	private session: Database = null;

	public constructor(session: Database) {
		this.session = session;
	}

	public async save(items: Car[]): Promise<Car[]> {
		let savedItems: Car[] = [];
		await this.session.car().load();
		for(let item of items) {
			let savedItem = await this.session.car().insert(item);
			savedItems.push(savedItem);
		}

		return savedItems;
	}

	public async update(item: Car): Promise<void> {
		await this.session.car().load();
		await this.session.car().update(item);
	}

	public async getByReport(reportId: string): Promise<Car[]> {
		await this.session.car().load();
		return await this.session.car().find({ reportId: reportId});
	}

	public async delete(id: string): Promise<void> {
		await this.session.car().load();
		await this.session.car().delete(id);
	}

	public async search(plates: string[]): Promise<Car[]> {
		await this.session.car().load();
		return await this.session.car().find({ plate: { $in: plates }});
	}
}
