import { Report } from '../domain/report';
import { Page } from '../dto/page';
import { Database } from '../database/database';

export class ReportDao {

	private session: Database = null;

	public constructor(session: Database) {
		this.session = session;
	}

	public async getPage(pageIndex: number): Promise<Page<Report>> {
		await this.session.report().load();
		let query = {};
		let total = await this.session.report().count(query);
		let items = await this.session.report().findPaged(query, pageIndex, { published: -1 });
		return {
			items: items,
			total: total
		};
	}

	public async save(item: Report): Promise<Report> {
		await this.session.report().load();
		let savedItem = await this.session.report().insert(item);
		return savedItem;
	}

	public async update(item: Report): Promise<void> {
		await this.session.report().load();
		await this.session.report().update(item);
	}

	public async getById(id: string): Promise<Report> {
		await this.session.report().load();
		return await this.session.report().findById(id);
	}

	public async delete(id: string): Promise<void> {
		await this.session.report().load();
		await this.session.car().load();
		await this.session.comment().load();
		await this.session.car().deleteBy({ reportId: id });
		await this.session.comment().deleteBy({ reportId: id });
		await this.session.report().delete(id);
	}
}
