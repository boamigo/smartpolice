import { Account } from '../domain/account';
import { Page } from '../dto/page';
import { Database } from '../database/database';

export class AccountDao {

	private session: Database = null;

	public constructor(session: Database) {
		this.session = session;
	}

	public async getById(id: string): Promise<Account> {
		await this.session.account().load();
		return await this.session.account().findById(id);
	}

	public async getByFb(fbId: string): Promise<Account> {
		await this.session.account().load();
		return await this.session.account().findOne({ fbId: fbId });
	}

	public async getByName(name: string): Promise<Account> {
		await this.session.account().load();
		return await this.session.account().findOne({ name: name });
	}

	public async getPage(pageIndex: number): Promise<Page<Account>> {
		await this.session.account().load();
		let query = {};
		let total = await this.session.account().count(query);
		let items = await this.session.account().findPaged(query, pageIndex, { name: 1 });
		return {
			items: items,
			total: total
		};
	}

	public async save(item: Account): Promise<Account> {
		await this.session.account().load();
		return this.session.account().insert(item);
	}

	public async delete(id: string): Promise<void> {
		await this.session.account().load();
		await this.session.comment().load();
		await this.session.comment().deleteBy({ accountId: id });
		await this.session.account().delete(id);
	}

	public async update(item: Account): Promise<void> {
		await this.session.account().load();
		await this.session.account().update(item);
	}
}
