import { Comment } from '../domain/comment';
import { Page } from '../dto/page';
import { Database } from '../database/database';

export class CommentDao {

	private session: Database = null;

	public constructor(session: Database) {
		this.session = session;
	}

	public async getByReport(reportId: string): Promise<Comment[]> {
		await this.session.comment().load();
		let query = { reportId: reportId };
		return await this.session.comment().find(query, { index: 1 });
	}

	public async save(item: Comment): Promise<Comment> {
		await this.session.comment().load();
		let savedItem = await this.session.comment().insert(item);
		return savedItem;
	}

	public async delete(id: string): Promise<void> {
		await this.session.comment().load();
		await this.session.comment().delete(id);
	}
}
