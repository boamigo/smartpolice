import { Request } from 'express';
import * as fs from 'fs';
import * as Jimp from 'jimp';
import { Context } from '../common/context';

export class FileHelper {

	async saveCarImage(req: Request, reportId: string, carId: string): Promise<void> {
		let files = req['files'];
		if (!files) {
			throw 'No file is uploaded';
		}

		let fileData = await Jimp.read(files.file.data);
		await this.resize(fileData, 600, Jimp.AUTO);
		await this.save(fileData, reportId, carId);
		await this.resize(fileData, 64, Jimp.AUTO);
		await this.save(fileData, reportId, carId + '_sm');
	}

	private resize(file: Jimp, h: number, w: number): Promise<void> {
		return new Promise<void>((resolve, reject)=> {
			file.resize(h, w, undefined, err => {
				if (err) {
					reject(err);
				}

				resolve();
			});
		});
	}

	private save(file: Jimp, folderName: string, fileName: string): Promise<void> {
		return new Promise<void>((resolve, reject)=> {
			file.write(Context.ROOT_DIR + '/images/' + folderName +'/' + fileName + '.jpg', err => {
				if (err) {
					reject(err);
				}

				resolve();
			});
		});
	}
}
