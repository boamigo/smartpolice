import * as moment from 'moment';

export class Helper {

	parseDate(date: string, time: string): Date {
		return moment(date + '-' + time, 'DD/MM/YYYY-HH:mm').toDate();
	}

	formatDate(value: Date): string {
		return moment(value).format('DD/MM/YYYY');
	}

	formatTime(value: Date): string {
		return moment(value).format('HH:mm');
	}
}
