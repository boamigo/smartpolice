import * as express from 'express';
import * as jwt from 'jsonwebtoken';

import { ReportService } from '../services/report-service';
import { AccountService } from '../services/account-service';
import { CommentService } from '../services/comment-service';

import { CarDao } from '../dao/car-dao';
import { ReportDao } from '../dao/report-dao';
import { AccountDao } from '../dao/account-dao';
import { CommentDao } from '../dao/comment-dao';

import { CommonMapper } from '../services/mappers/helpers/common-mapper';
import { CarMapper } from '../services/mappers/car-mapper';
import { ReportMapper } from '../services/mappers/report-mapper';
import { AccountMapper } from '../services/mappers/account-mapper';
import { CommentMapper } from '../services/mappers/comment-mapper';

import { FileHelper } from '../common/file-helper';
import { Database } from '../database/database';
import { Helper } from './helper';

export class Context {

	public static ROOT_DIR;
	public authenticated: boolean = false;
	public user: string = null;

	constructor(
		private currentRequest: express.Request,
		private currentResponse: express.Response) {
	}

	public static setupContext(req: express.Request, res: express.Response): Promise<Context> {
		let context = new Context(req, res);
		req['context'] = context;

		let token = req.query.token || req.headers['x-access-token'];
		return new Promise<Context>((resolve, reject) => {
			if (!token) {
				return resolve(context);
			}

			jwt.verify(token, 'SMARTPOLICE_PWD', (err, decoded) => {
				if (err) {
					resolve(context);
					return
				}
	
				context.user = decoded.name;
				context.authenticated = true;
				resolve(context);
			});
		});
	}

	public static getCurrentContext(req: express.Request): Context {
		return <Context>req['context'];
	}

	public static validateUser(req: express.Request): boolean {
		if ('vhusnullin' === req.body.name && '21327abC' === req.body.pwd) {
			return true;
		}

		return false;
	}

	public static issueToken(req: express.Request): string {
		return jwt.sign({ name: req.body.name }, 'SMARTPOLICE_PWD', { expiresIn: 1800 });
	}

	assertAdmin(): void {
		if (this.user !== 'vhusnullin') {
			this.currentResponse.status(403).send('Forbidden');
			throw 'Forbidden';
		}
	}

	public getFileHelper(): FileHelper {
		return new FileHelper();
	}

	public reportService(): ReportService {
		let session = new Database();
		let commonMapper = new CommonMapper(new Helper());
		let carDao = new CarDao(session);
		return new ReportService(
			new ReportDao(session),
			carDao,
			new ReportMapper(commonMapper, carDao),
			new CarMapper(carDao, commonMapper));
	}

	public accountService(): AccountService {
		let session = new Database();
		return new AccountService(
			new AccountDao(session),
			new AccountMapper());
	}

	public commentService(): CommentService {
		let session = new Database();
		let accountDao = new AccountDao(session);
		return new CommentService(
			new CommentDao(session),
			accountDao,
			new CommentMapper(accountDao));
	}
}
