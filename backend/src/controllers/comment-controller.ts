import { Router, Request, Response, NextFunction } from 'express';
import { Context } from '../common/context';

let router = Router();
router.get("/getbyreport/:reportId", async (req: Request, res: Response, next: NextFunction) => {
	try {
		let context = Context.getCurrentContext(req);
		let comments = await context.commentService().getByReport(req.params.reportId);
		res.status(200).json(comments);
	}
	catch(err) {
		next(err);
	}
});

router.post("/save", async (req: Request, res: Response, next: NextFunction) => {
	try {
		let context = Context.getCurrentContext(req);
		let account = await context.commentService().save(req.body);
		res.status(200).json(account);
	}
	catch(err) {
		next(err);
	}
});

router.delete("/delete/:id", async (req: Request, res: Response, next: NextFunction) => {
	try {
		let context = Context.getCurrentContext(req);
		context.assertAdmin();
		await context.commentService().delete(req.params.id);
		res.status(200).end();
	}
	catch(err) {
		next(err);
	}
});

export default router;
