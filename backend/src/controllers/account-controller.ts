import { Router, Request, Response, NextFunction } from 'express';
import { Context } from '../common/context';

let router = Router();
router.get("/getpage/:pageIndex", async (req: Request, res: Response, next: NextFunction) => {
	try {
		let context = Context.getCurrentContext(req);
		context.assertAdmin();
		let pageIndex = req.params.pageIndex ? Number(req.params.pageIndex) : 0;
		let page = await context.accountService().getPage(pageIndex);
		res.status(200).json(page);
	}
	catch(err) {
		next(err);
	}
});

router.post("/save", async (req: Request, res: Response, next: NextFunction) => {
	try {
		let context = Context.getCurrentContext(req);
		let account = await context.accountService().save(req.body);
		res.status(200).json(account);
	}
	catch(err) {
		if (err === 'duplicate') {
			res.status(520).end();
			return;
		}

		next(err);
	}
});

router.get("/getbyfb/:fbId", async (req: Request, res: Response, next: NextFunction) => {
	try {
		let context = Context.getCurrentContext(req);
		let item = await context.accountService().getByFb(req.params.fbId)
		if (!item) {
			res.status(200).end();
			return;
		}

		res.status(200).json(item);
	}
	catch(err) {
		next(err);
	}
});

router.delete("/delete/:id", async (req: Request, res: Response, next: NextFunction) => {
	try {
		let context = Context.getCurrentContext(req);
		context.assertAdmin();
		await context.accountService().delete(req.params.id);
		res.status(200).end();
	}
	catch(err) {
		next(err);
	}
});

export default router;
