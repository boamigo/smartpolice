import { Router, Request, Response, NextFunction } from 'express';
import { Context } from '../common/context';
import { FileHelper } from '../common/file-helper';

let router = Router();
router.get("/getpage/:pageIndex", async (req: Request, res: Response, next: NextFunction) => {
	try {
		let context = Context.getCurrentContext(req);
		let pageIndex = req.params.pageIndex ? Number(req.params.pageIndex) : 0;
		let page = await context.reportService().getPage(pageIndex);
		res.status(200).json(page);
	}
	catch(err) {
		next(err);
	}
});

router.get("/getbyid/:reportId", async (req: Request, res: Response, next: NextFunction) => {
	try {
		if (!req.params.reportId) {
			res.status(500).end();
			return;
		}

		let context = Context.getCurrentContext(req);
		context.assertAdmin();
		let report = await context.reportService().getById(req.params.reportId);
		res.status(200).json(report);
	}
	catch(err) {
		next(err);
	}
});

router.post("/publish", async (req: Request, res: Response, next: NextFunction) => {
	try {
		if (!req.body) {
			res.status(500).end();
			return;
		}

		let context = Context.getCurrentContext(req);
		let report = await context.reportService().save(req.body);
		res.status(200).json(report);
	}
	catch(err) {
		next(err);
	}
});

router.post("/upload", async (req: Request, res: Response, next: NextFunction) => {
	try {
		if (!req.body.reportId || !req.body.carId) {
			res.status(500).end();
			return;
		}

		let context = Context.getCurrentContext(req);
		await context.getFileHelper().saveCarImage(req, req.body.reportId, req.body.carId);
		res.status(200).end();
	}
	catch(err) {
		next(err);
	}
});

router.delete("/delete/:reportId", async (req: Request, res: Response, next: NextFunction) => {
	try {
		if (!req.params.reportId) {
			res.status(500).end();
			return;
		}

		let context = Context.getCurrentContext(req);
		context.assertAdmin();
		await context.reportService().delete(req.params.reportId);
		res.status(200).end();
	}
	catch(err) {
		next(err);
	}
});

router.delete("/deletecar/:carId", async (req: Request, res: Response, next: NextFunction) => {
	try {
		if (!req.params.carId) {
			res.status(500).end();
			return;
		}

		let context = Context.getCurrentContext(req);
		context.assertAdmin();
		await context.reportService().deleteCar(req.params.carId);
		res.status(200).end();
	}
	catch(err) {
		next(err);
	}
});

router.get("/searchcar/:plate", async (req: Request, res: Response, next: NextFunction) => {
	try {
		if (!req.params.plate) {
			res.status(500).end();
			return;
		}

		let context = Context.getCurrentContext(req);
		let cars = await context.reportService().searchCar(req.params.plate);
		res.status(200).json(cars);
	}
	catch(err) {
		next(err);
	}
});

router.post("/update", async (req: Request, res: Response, next: NextFunction) => {
	try {
		if (!req.body) {
			res.status(500).end();
			return;
		}

		let context = Context.getCurrentContext(req);
		let report = await context.reportService().update(req.body);
		res.status(200).json(report);
	}
	catch(err) {
		next(err);
	}
});

export default router;
