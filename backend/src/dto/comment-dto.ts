export interface CommentDto {
	id: string;
	text: string;
	accountId: string;
	account: string;
	reportId: string;
	index: number;
}
