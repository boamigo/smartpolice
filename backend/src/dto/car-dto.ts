export interface CarDto {
	id: string;
	plate: string;
	brand: string;
	reportId?: string;
}
