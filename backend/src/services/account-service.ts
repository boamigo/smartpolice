import { AccountDto } from '../dto/account-dto';
import { AccountDao } from '../dao/account-dao';
import { Account } from '../domain/account';
import { Page } from '../dto/page';
import { AccountMapper } from './mappers/account-mapper';

export class AccountService {

	public constructor(
		private accountDao: AccountDao,
		private accountMapper: AccountMapper) {
	}

	public async getPage(pageIndex: number): Promise<Page<AccountDto>> {
		let page = await this.accountDao.getPage(pageIndex);
		return this.accountMapper.toDtoPage(page);
	}

	public async save(item: AccountDto): Promise<AccountDto> {
		let domain = await this.accountDao.getByFb(item.fbId);
		if (domain) {
			this.accountMapper.mapUpdate(item, domain);
			await this.accountDao.update(domain);
		}
		else {
			domain = await this.accountDao.getByName(item.name);
			if (domain) {
				throw 'duplicate';
			}

			domain = this.accountMapper.toDomain(item);
			domain = await this.accountDao.save(domain);
		}

		return this.accountMapper.toDto(domain);
	}

	public async getByFb(fbId: string): Promise<AccountDto> {
		let item = await this.accountDao.getByFb(fbId);
		if (!item) {
			return null;
		}

		return this.accountMapper.toDto(item);
	}

	public async delete(id: string): Promise<void> {
		return this.accountDao.delete(id);
	}
}
