import { CommentDto } from '../dto/comment-dto';
import { CommentDao } from '../dao/comment-dao';
import { AccountDao } from '../dao/account-dao';
import { CommentMapper } from './mappers/comment-mapper';
import { CarMapper } from './mappers/car-mapper';

export class CommentService {

	public constructor(
		private commentDao: CommentDao,
		private accountDao: AccountDao,
		private commentMapper: CommentMapper) {
	}

	public async getByReport(reportId: string): Promise<CommentDto[]> {
		let comments = await this.commentDao.getByReport(reportId);
		return await this.commentMapper.toDtoList(comments);
	}

	public async save(dto: CommentDto): Promise<void> {
		let account = await this.accountDao.getById(dto.accountId);
		if (!account) {
			throw 'no account';
		}

		let comments = await this.commentDao.getByReport(dto.reportId);
		if (comments.length > 20) {
			throw 'too many comments';
		}

		let domain = this.commentMapper.toDomain(dto, account, comments.length);
		await this.commentDao.save(domain);
	}

	public async delete(commentId: string): Promise<void> {
		await this.commentDao.delete(commentId);
	}
}
