import { ReportDto } from '../dto/report-dto';
import { Page } from '../dto/page';
import { ReportDao } from '../dao/report-dao';
import { CarDao } from '../dao/car-dao';
import { ReportMapper } from './mappers/report-mapper';
import { CarMapper } from './mappers/car-mapper';
import { CarDto } from '../dto/car-dto';

export class ReportService {

	public constructor(
		private reportDao: ReportDao,
		private carDao: CarDao,
		private repportMapper: ReportMapper,
		private carMapper: CarMapper) {
	}

	public async getPage(pageIndex: number): Promise<Page<ReportDto>> {
		let page = await this.reportDao.getPage(pageIndex);
		return this.repportMapper.toDtoPage(page);
	}

	public async getById(id: string): Promise<ReportDto> {
		let item = await this.reportDao.getById(id);
		return this.repportMapper.toDto(item);
	}

	public async save(dto: ReportDto): Promise<ReportDto> {
		let domain = this.repportMapper.toDomain(dto);
		let savedReport = await this.reportDao.save(domain);
		let cars = this.carMapper.toDomain(dto.cars, savedReport);
		let savedCars = await this.carDao.save(cars);
		return this.repportMapper.toSaveResponseDto(savedReport, savedCars);
	}

	public async update(dto: ReportDto): Promise<void> {
		let existingReport = await this.reportDao.getById(dto.id);
		this.repportMapper.toDomainForUpdate(dto, existingReport);
		await this.reportDao.update(existingReport);
		let existingCars = await this.carDao.getByReport(dto.id);
		this.carMapper.merge(dto.cars, existingCars);
	}

	public async delete(reportId: string): Promise<void> {
		await this.reportDao.delete(reportId);
	}

	public async deleteCar(carId: string): Promise<void> {
		await this.carDao.delete(carId);
	}

	public async searchCar(plate: string): Promise<CarDto[]> {
		let plates = this.carMapper.getPlateVariants(plate);
		let cars = await this.carDao.search(plates);
		return this.carMapper.toDtoList(cars);
	}
}
