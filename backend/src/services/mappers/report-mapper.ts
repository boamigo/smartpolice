import { Report } from '../../domain/report';
import { Car } from '../../domain/car';
import { ReportDto } from '../../dto/report-dto';
import { Page } from '../../dto/page';
import { CommonMapper } from './helpers/common-mapper';
import { CarDao } from '../../dao/car-dao';

export class ReportMapper {

	public constructor(
		private commonMapper: CommonMapper,
		private carDao: CarDao) {
	}

	public toDomain(report: ReportDto): Report {
		return this.commonMapper.reportToDomain(report);
	}

	public toDomainForUpdate(from: ReportDto, to: Report): void {
		this.commonMapper.reportToExistingDomain(from, to);
	}

	public async toDtoPage(page: Page<Report>): Promise<Page<ReportDto>> {
		let reportDtoList: ReportDto[] = [];
		for(let report of page.items) {
			let reportDto = this.commonMapper.reportToDto(report);
			reportDtoList.push(reportDto);
			let items = await this.carDao.getByReport(report._id);
			let itemDtoList = this.commonMapper.carsToDto(items);
			reportDto.cars = itemDtoList;
		}

		return {
			items: reportDtoList,
			total: page.total
		};
	}

	public async toDto(report: Report): Promise<ReportDto> {
		let reportDto = this.commonMapper.reportToDto(report);
		let cars = await this.carDao.getByReport(report._id);
		reportDto.cars = this.commonMapper.carsToDto(cars);
		return reportDto;
	}

	public async toSaveResponseDto(report: Report, cars: Car[]): Promise<ReportDto> {
		let reportDto: ReportDto = {
			id: report._id,
			address: undefined,
			date: undefined,
			time: undefined,
			notes: undefined,
			cars: []
		};
		for(let car of cars){
			reportDto.cars.push({
				id: car._id,
				plate: undefined,
				brand: undefined
			});
		}

		return reportDto;
	}
}
