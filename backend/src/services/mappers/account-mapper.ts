import { Account } from '../../domain/account';
import { AccountDto } from '../../dto/account-dto';
import { Page } from '../../dto/page';

export class AccountMapper {

	public constructor() {
	}

	public async toDtoPage(page: Page<Account>): Promise<Page<AccountDto>> {
		let dtoList: AccountDto[] = [];
		for(let item of page.items) {
			dtoList.push({
				id: item._id,
				name: item.name,
				fbId: item.fbId
			});
		}

		return {
			items: dtoList,
			total: page.total
		};
	}

	public async toDto(item: Account): Promise<AccountDto> {
		let dto: AccountDto = {
			id: item._id,
			name: item.name,
			fbId: item.fbId
		};

		return dto;
	}

	public toDomain(item: AccountDto): Account {
		let domain: Account = {
			_id: undefined,
			name: item.name,
			fbId: item.fbId
		};

		return domain;
	}

	public mapUpdate(from: AccountDto, to: Account): void {
		to.name = from.name;
	}
}
