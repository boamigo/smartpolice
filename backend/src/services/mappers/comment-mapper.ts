import { Comment } from '../../domain/comment';
import { Account } from '../../domain/account';
import { CommentDto } from '../../dto/comment-dto';
import { AccountDao } from '../../dao/account-dao';

export class CommentMapper {

	public constructor(
		private accountDao: AccountDao) {
	}

	public toDomain(dto: CommentDto, account: Account, index: number): Comment {
		return {
			_id: undefined,
			text: dto.text,
			accountId: account._id,
			reportId: dto.reportId,
			index: index
		};
	}

	public async toDtoList(comments: Comment[]): Promise<CommentDto[]> {
		let dtoList: CommentDto[] = [];
		let accounts: Account[] = [];
		for(let comment of comments) {
			let account = accounts.find(o => o._id === comment.accountId);
			if (!account) {
				account = await this.accountDao.getById(comment.accountId);
				if (!account) {
					continue;
				}
			}

			let dto: CommentDto = {
				id: undefined,
				text: comment.text,
				accountId: undefined,
				account: account.name,
				reportId: undefined,
				index: undefined,
			};
			dtoList.push(dto);
		}

		return dtoList;
	}
}
