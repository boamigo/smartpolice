import { Car } from '../../domain/car';
import { Report } from '../../domain/report';
import { CarDto } from '../../dto/car-dto';
import { Page } from '../../dto/page';
import { CommonMapper } from './helpers/common-mapper';
import { CarDao } from '../../dao/car-dao';

export class CarMapper {

	public constructor(
		private carDao: CarDao,
		private commonMapper: CommonMapper) {
	}

	public toDomain(items: CarDto[], report: Report): Car[] {
		let list: Car[] = [];
		for(let item of items) {
			list.push({
				_id: undefined,
				plate: item.plate,
				brand: item.brand,
				reportId: report._id
			});
		}

		return list;
	}

	public toDtoList(items: Car[]): CarDto[] {
		let list: CarDto[] = [];
		for(let item of items) {
			list.push({
				id: item._id,
				brand: item.brand,
				plate: item.plate,
				reportId: item.reportId
			});
		}

		return list;
	}

	public merge(dtoList: CarDto[], domainList: Car[]): void {
		for(let domain of domainList) {
			let matchDto = dtoList.find(o => o.id === domain._id);
			if (!matchDto) {
				this.carDao.delete(domain._id);
			}

			this.commonMapper.carToExistingDomain(matchDto, domain);
			this.carDao.update(domain);
		}
	}

	public getPlateVariants(plate: string): string[] {
		let plates: string[] = [];
		plates.push(plate);
		if (plate.length <= 6) {
			plates.push(plate[0] + ' ' + plate.substr(1)); // H 3344
			plates.push(plate.substr(0, 3) + ' ' + plate.substr(3)); // VXL 345, VXL 10, VXL 4
			plates.push(plate.substr(0, 1) + ' ' + plate.substr(1, 2) + ' ' + plate.substr(3)); // C RP 390
		}

		if (plate.length === 7) {
			plates.push(plate.substr(0, 2) + ' ' + plate.substr(2, 2) + ' ' + plate.substr(4)); // OR BR 111
			plates.push(plate.substr(0, 2) + ' ' + plate.substr(2, 3) + ' ' + plate.substr(5)); // CD 112 AA
			plates.push(plate.substr(0, 3) + ' ' + plate.substr(3)); // MAI 9859
		}

		return plates;
	}
}
