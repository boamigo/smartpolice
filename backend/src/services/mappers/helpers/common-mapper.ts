import { Car } from '../../../domain/car';
import { Report } from '../../../domain/report';
import { CarDto } from '../../../dto/car-dto';
import { ReportDto } from '../../../dto/report-dto';
import { Helper } from '../../../common/helper';

export class CommonMapper {

	public constructor(
		private helper: Helper) {
	}

	carsToDto(domains: Car[]): CarDto[] {
		let dtos: CarDto[] = [];
		for(let domain of domains) {
			dtos.push({
				id: domain._id,
				plate: domain.plate,
				brand: domain.brand
			});
		}

		return dtos;
	}

	carToDomain(dto: CarDto, reportId: string): Car {
		return {
			_id: undefined,
			plate: dto.plate,
			brand: dto.brand,
			reportId: reportId
		};
	}

	carToExistingDomain(dto: CarDto, domain: Car): void {
		domain.brand = dto.brand;
		domain.plate = dto.plate;
	}

	reportToDto(domain: Report): ReportDto {
		return {
			id: domain._id,
			address: domain.address,
			date: this.helper.formatDate(domain.date),
			time: this.helper.formatTime(domain.date),
			notes: domain.notes,
			cars: []
		};
	}

	reportToDomain(dto: ReportDto): Report {
		return {
			_id: undefined,
			address: dto.address,
			notes: dto.notes,
			date: this.helper.parseDate(dto.date, dto.time),
			published: new Date(),
			show: false
		};
	}

	reportToExistingDomain(dto: ReportDto, domain: Report): void {
		domain.address = dto.address;
		domain.notes = dto.notes;
		domain.date = this.helper.parseDate(dto.date, dto.time);
	}
}
