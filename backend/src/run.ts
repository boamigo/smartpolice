import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as fileUpload from 'express-fileupload';
import reportController from './controllers/report-controller';
import accountController from './controllers/account-controller';
import commentController from './controllers/comment-controller';
import { Context } from './common/context';
import { AddressInfo } from 'net';

Context.ROOT_DIR = __dirname;
let app: express.Application = express();
app.use(bodyParser.json());
app.use(fileUpload());
app.use('/', express.static(Context.ROOT_DIR + '/images'));
app.use('/', express.static(Context.ROOT_DIR + '/www'));

app.post('/token', (req: express.Request, res: express.Response, next: express.NextFunction) => {
	let validUser = Context.validateUser(req);
	if (!validUser) {
		res.status(403).send('Forbidden');
		return;
	}

	let token = Context.issueToken(req);
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	return res.status(200).send(token);
});

app.use(async (req: express.Request, res: express.Response, next: express.NextFunction) => {
	if (!req.url.startsWith('/api')) {
		return res.sendFile(__dirname + '/www/index.html');
	}

	let context = await Context.setupContext(req, res)
	.then(() => {
		if ('GET' === req.method) {
			res.header('Access-Control-Allow-Origin', '*');
			res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
		}

		next();
	});
});

app.use('/api/report', reportController);
app.use('/api/account', accountController);
app.use('/api/comment', commentController);

//app.use((req: express.Request, res: express.Response) => {
//	res.send('error');
//});

process.env.PORT = '8181';
let server = app.listen(process.env.PORT, () => {
	console.log('listening at http://localhost:' + (server.address() as AddressInfo).port);
});
