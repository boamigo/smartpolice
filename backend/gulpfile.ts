const gulp = require('gulp');
const del = require('del');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const tsProject = ts.createProject('tsconfig.json', {
	typescript: require('typescript')
});

gulp.task('clean', cb => {
	return del(['src/**/*.js', '!src/www/**'], cb);
});

gulp.task('compile', ['clean'], () => {
	var tsResult = gulp.src('src/**/*.ts')
		.pipe(sourcemaps.init())
		.pipe(tsProject());
	return tsResult.js
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('src'));
});

gulp.task('deploy_clean', cb => {
	return del(['deploy/src/**/*.js', 'deploy/src/**/*.html'], cb);
});

gulp.task('deploy_compile', ['deploy_clean'], () => {
	var tsResult = gulp.src('src/**/*.ts')
		.pipe(tsProject());
	return tsResult.js
		.pipe(gulp.dest('deploy/src'));
});
